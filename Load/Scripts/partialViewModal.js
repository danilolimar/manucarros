﻿
$(document).ready(function () {
    $('#datatable').DataTable({
        dom: 'Bfrtip',
        buttons: [
         
            'excelHtml5'
           
        ]
    });

    

$("#exportToExcel").click(function (e) {
    $("#datatableGrid").table2excel({
        filename: $("#exportToExcel").data("target"),
        fileext: ".xls"
    });
});

$(".create").click(function () {
    var id = $(this).attr("data-id");
    var url = window.location.href;
    $("#modal").load(url + '/Create', function () {
            $("form").attr("autocomplete", "off");
        $("#modal").modal();
    });
});
$(".details").click(function () {
    var id = $(this).attr("data-id");
    var url = window.location.href;
    $("#modal").load(url + '/Details?id=' + id, function () {
        $("#modal").modal();
    });
});
$(".edit").click(function () {
    var id = $(this).attr("data-id");
    var url = window.location.href;
    $("#modal").load(url + '/Edit?id=' + id, function () {
        $("form").attr("autocomplete", "off");
        $("#modal").modal();
    });
});
$(".delete").click(function () {
    var id = $(this).attr("data-id");
    var url = window.location.href;
    $("#modal").load(url + '/Delete?id=' + id, function () {
        $("#modal").modal();
    });
});

$(".load").click(function () {
    var id = $(this).attr("data-id");
    var url = window.location.href;
    $("#modal").load(url + '/Load?id=' + id, function () {
        $("form").attr("autocomplete", "off");
        $("#modal").modal();
    });
});


    });
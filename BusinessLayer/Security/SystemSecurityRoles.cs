﻿using System.Web;
using System.Web.Mvc;

namespace BusinessLayer.Security
{
    public static class SystemSecurityRoles
    {
        public static bool IsInRole(string pRole)
        {
            return HttpContext.Current.User.IsInRole("SUPORTE") || HttpContext.Current.User.IsInRole(pRole);
        }
    }
}
﻿//using BusinessLayer.Commom;
//using Microsoft.Exchange.WebServices.Data;
//using System;
//using System.Collections.Generic;
//using System.DirectoryServices;
//using System.DirectoryServices.AccountManagement;
//using System.Linq;
//using System.Net;


//namespace BusinessLayer.Security
//{
//    public class EmployeeInformation
//    {

//        public String SamAccountName { get; set; }
//        public String DisplayName { get; set; }
//        public String Name { get; set; }
//        public String GivenName { get; set; }
//        public String Surname { get; set; }
//        public String Description { get; set; }
//        public Boolean? userAccountControl { get; set; }
//        //public DateTime? LastLogon { get; set; }
//        public List<String> Groups { get; set; }
//        public String Path { get; set; }
//        public String Workday { get; set; }
//        public String Shift { get; set; }
//        public String City { get; set; }
//        public String StateProvinc { get; set; }
//        public String CountryRegionCode { get; set; }
//        public String Telephonenumber { get; set; }
//        public String division { get; set; }
//        public String manager { get; set; }
//        public String HostName { get; set; }
//        public String mail { get; set; }

//        public String Site { get; set; }

//       // public String photo { get; set; }

//        public EmployeeInformation(string userName, string password)
//        {
//            SearchResult info = LdapAuthentication.GetLdapAuthenticationInfo(userName, password);
//            try
//            {
//                if (info.Properties.Contains("extensionAttribute13"))
//                {
//                    this.Site = info.Properties["extensionAttribute13"][0].TrimToUpper().Split('_')[0] + " - " + info.Properties["physicaldeliveryofficename"][0].TrimToUpper();
//                }
//                else
//                {
//                    this.Site = "INFORMATION NOT FOUND IN LDAP";
//                }

//                this.SamAccountName = info.Properties["SamAccountName"].Count > 0 ? info.Properties["SamAccountName"][0].ToString() : ""; // ad_user.SamAccountName;
//                this.DisplayName = info.Properties["DisplayName"].Count > 0  ? info.Properties["DisplayName"][0].ToString() : "";
//                this.Name = info.Properties["Name"].Count > 0 ? info.Properties["Name"][0].ToString() : "";
//                this.GivenName = info.Properties["GivenName"].Count > 0 ? info.Properties["GivenName"][0].ToString() : "";
//                this.division = info.Properties["division"].Count > 0 ? info.Properties["division"][0].ToString()  : "";
//                this.Surname = info.Properties["sn"].Count > 0 ? info.Properties["sn"][0].ToString() : "";
//                this.Description = info.Properties["Description"].Count > 0 ? info.Properties["Description"][0].ToString() : "";
//                this.userAccountControl = info.Properties["userAccountControl"][0].ToString().Trim() == "512" ? true : false;
//               // this.LastLogon = Utils.ConvertToTimestamp( Convert.ToInt64(info.Properties["lastlogontimestamp"][0]));
//                this.Path = info.Path.Count() > 0 ?  info.Path.Split('C')[0].Split('/')[2] : "";
//                this.Workday = info.Properties["employeenumber"].Count > 0 ? info.Properties["employeenumber"][0].ToString() : "";
//                this.Shift = info.Properties["title"].Count > 0 ? info.Properties["title"][0].ToString() : "";
//                this.City = info.Properties["l"].Count > 0 ? info.Properties["l"][0].ToString() : "";
//                this.StateProvinc = info.Properties["st"].Count > 0 ? info.Properties["st"][0].ToString() : "";
//                this.CountryRegionCode = info.Properties["c"].Count > 0 ? info.Properties["c"][0].ToString() : "";
//                this.Telephonenumber = info.Properties["Telephonenumber"].Count > 0 ? info.Properties["Telephonenumber"][0].ToString(): "";
//                this.manager = info.Properties["manager"].Count > 0 ? info.Properties["manager"][0].ToString(): "";
//                this.manager = manager == "" ? manager.Remove(manager.IndexOf(",OU")).Replace("CN=",""): "";
//                this.HostName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().HostName.ToString();
//                this.mail = info.Properties["mail"].Count > 0 ? info.Properties["mail"][0].TrimToUpper() : "";
//                this.Groups = GetUserGroups(Path, this.DisplayName);
//            }
//            catch
//            {
                
//            }
//            //this.photo = ProfileImage(userName, password);
//        }
//        public EmployeeInformation(string userName)
//        {
//            DirectoryEntry entry = new DirectoryEntry("LDAP://"+ AdCorrectLink(userName).ConnectedServer);
//            using (DirectorySearcher ds = new DirectorySearcher(entry))
//            {
              
//                ds.Filter = "(&(samaccountname="+ userName + "))";

//                SearchResultCollection results = ds.FindAll();

//                foreach (SearchResult info in results)
//                {
//                    if (info.Properties.Contains("extensionAttribute13"))
//                    {
//                        this.Site = info.Properties["extensionAttribute13"][0].TrimToUpper().Split('_')[0] + " - " + info.Properties["physicaldeliveryofficename"][0].TrimToUpper();
//                    }
//                    else
//                    {
//                        this.Site = "INFORMATION NOT FOUND IN LDAP";
//                    }
//                    this.SamAccountName = info.Properties["SamAccountName"].Count > 0 ? info.Properties["SamAccountName"][0].ToString() : ""; // ad_user.SamAccountName;
//                    this.DisplayName = info.Properties["DisplayName"].Count > 0 ? info.Properties["DisplayName"][0].ToString() : "";
//                    this.Name = info.Properties["Name"].Count > 0 ? info.Properties["Name"][0].ToString() : "";
//                    this.GivenName = info.Properties["GivenName"].Count > 0 ? info.Properties["GivenName"][0].ToString() : "";
//                    this.division = info.Properties["division"].Count > 0 ? info.Properties["division"][0].ToString() : "";
//                    this.Surname = info.Properties["sn"].Count > 0 ? info.Properties["sn"][0].ToString() : "";
//                    this.Description = info.Properties["Description"].Count > 0 ? info.Properties["Description"][0].ToString() : "";
//                    this.userAccountControl = info.Properties["userAccountControl"][0].ToString().Trim() == "512" ? true : false;
//                    // this.LastLogon = Utils.ConvertToTimestamp( Convert.ToInt64(info.Properties["lastlogontimestamp"][0]));
//                    this.Path = info.Path.Count() > 0 ? info.Path.Split('C')[0].Split('/')[2] : "";
//                    this.Workday = info.Properties["employeenumber"].Count > 0 ? info.Properties["employeenumber"][0].ToString() : "";
//                    this.Shift = info.Properties["title"].Count > 0 ? info.Properties["title"][0].ToString() : "";
//                    this.City = info.Properties["l"].Count > 0 ? info.Properties["l"][0].ToString() : "";
//                    this.StateProvinc = info.Properties["st"].Count > 0 ? info.Properties["st"][0].ToString() : "";
//                    this.CountryRegionCode = info.Properties["c"].Count > 0 ? info.Properties["c"][0].ToString() : "";
//                    this.Telephonenumber = info.Properties["Telephonenumber"].Count > 0 ? info.Properties["Telephonenumber"][0].ToString() : "";
//                    this.manager = info.Properties["manager"].Count > 0 ? info.Properties["manager"][0].ToString() : "";
//                    this.manager = manager == "" ? manager.Remove(manager.IndexOf(",OU")).Replace("CN=", "") : "";
//                    this.HostName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().HostName.ToString();
//                    this.mail = info.Properties["mail"].Count > 0 ? info.Properties["mail"][0].TrimToUpper() : "";
//                    this.Groups = GetUserGroups(Path, this.DisplayName);
       
//                }
//            }
//        }

//        //  public List<string> Properties()
//        //  {
//        //      return new List<string> { SamAccountName, DisplayName, Name,
//        //    GivenName, Surname, Description, Enabled.ToString(), LastLogon.ToString() };
//        //  }
//        //  public int UserPropertiesTotal = 8;
//        //  public static string[] StringArrayUesrProperties = { "SamAccountName",
//        //"DisplayName", "Name", "GivenName", "Surname",
//        //"Description", "Enabled", "LastLogon" };

//        public static List<string> GetUserGroups(string domainDN, string sAMAccountName)
//        {
//            List<string> lGroups = new List<string>();
//            try
//            {
//                //Create the DirectoryEntry object to bind the distingusihedName of your domain
//                using (DirectoryEntry rootDE = new DirectoryEntry("LDAP://" + domainDN))
//                {
//                    //Create a DirectorySearcher for performing a search on abiove created DirectoryEntry
//                    using (DirectorySearcher dSearcher = new DirectorySearcher(rootDE))
//                    {
//                        //Create the sAMAccountName as filter
//                        dSearcher.Filter = "(&(CN=" + sAMAccountName + ")(objectClass=User)(objectCategory=Person))";
//                        dSearcher.PropertiesToLoad.Add("memberOf");
//                        dSearcher.ClientTimeout.Add(new TimeSpan(0, 20, 0));
//                        dSearcher.ServerTimeLimit.Add(new TimeSpan(0, 20, 0));

//                        //Search the user in AD
//                        SearchResult sResult = dSearcher.FindOne();
//                        if (sResult == null)
//                        {
//                            throw new ApplicationException("No user with username " + sAMAccountName + " could be found in the domain");
//                        }
//                        else
//                        {
//                            //Once we get the userm let us get all the memberOF attibute's value
//                            foreach (var grp in sResult.Properties["memberOf"])
//                            {
//                                string sGrpName = Convert.ToString(grp).Remove(0, 3);
//                                //Bind to this group
//                                DirectoryEntry deTempForSID = new DirectoryEntry("LDAP://" + grp.ToString().Replace("/", "\\/"));
//                                try
//                                {
//                                    deTempForSID.RefreshCache();

//                                    //Get the objectSID which is Byte array
//                                    byte[] objectSid = (byte[])deTempForSID.Properties["objectSid"].Value;

//                                    //Pass this Byte array to Security.Principal.SecurityIdentifier to convert this
//                                    //byte array to SDDL format
//                                    System.Security.Principal.SecurityIdentifier SID = new System.Security.Principal.SecurityIdentifier(objectSid, 0);

//                                    if (sGrpName.Contains(",CN"))
//                                    {
//                                        sGrpName = sGrpName.Remove(sGrpName.IndexOf(",CN"));
//                                    }
//                                    else if (sGrpName.Contains(",OU"))
//                                    {
//                                        sGrpName = sGrpName.Remove(sGrpName.IndexOf(",OU"));
//                                    }

//                                    //Perform a recursive search on these groups.
//                                    RecursivelyGetGroups(dSearcher, lGroups, sGrpName, SID.ToString());
//                                }
//                                catch (Exception ex)
//                                {
//                                    Console.WriteLine("Error while binding to path : " + grp.ToString());
//                                    Console.WriteLine(ex.Message.ToString());
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                //Console.WriteLine("Please check the distinguishedName of the domain if it is as per your domain or not?");
//                //Console.WriteLine(ex.Message.ToString());
//                //System.Environment.Exit(0);
//            }
//            return lGroups;
//        }

//        public static void RecursivelyGetGroups(DirectorySearcher dSearcher, List<string> lGroups, string sGrpName, string SID)
//        {
//            //Check if the group has already not found
//            if (!lGroups.Contains(sGrpName))
//            {
//                //lGroups.Add(sGrpName + " : " + SID);
//                lGroups.Add(sGrpName);
//                //Now perform the search based on this group
//                dSearcher.Filter = "(&(objectClass=grp)(CN=" + sGrpName + "))".Replace("\\", "\\\\");
//                dSearcher.ClientTimeout.Add(new TimeSpan(0, 2, 0));
//                dSearcher.ServerTimeLimit.Add(new TimeSpan(0, 2, 0));

//                //Search this group
//                SearchResult GroupSearchResult = dSearcher.FindOne();
//                if ((GroupSearchResult != null))
//                {
//                    foreach (var grp in GroupSearchResult.Properties["memberOf"])
//                    {

//                        string ParentGroupName = Convert.ToString(grp).Remove(0, 3);

//                        //Bind to this group
//                        DirectoryEntry deTempForSID = new DirectoryEntry("LDAP://" + grp.ToString().Replace("/", "\\/"));
//                        try
//                        {
//                            //Get the objectSID which is Byte array
//                            byte[] objectSid = (byte[])deTempForSID.Properties["objectSid"].Value;

//                            //Pass this Byte array to Security.Principal.SecurityIdentifier to convert this
//                            //byte array to SDDL format
//                            System.Security.Principal.SecurityIdentifier ParentSID = new System.Security.Principal.SecurityIdentifier(objectSid, 0);

//                            if (ParentGroupName.Contains(",CN"))
//                            {
//                                ParentGroupName = ParentGroupName.Remove(ParentGroupName.IndexOf(",CN"));
//                            }
//                            else if (ParentGroupName.Contains(",OU"))
//                            {
//                                ParentGroupName = ParentGroupName.Remove(ParentGroupName.IndexOf(",OU"));
//                            }
//                            RecursivelyGetGroups(dSearcher, lGroups, ParentGroupName, ParentSID.ToString());
//                        }
//                        catch (Exception ex)
//                        {
//                            //Console.WriteLine("Error while binding to path : " + grp.ToString());
//                            //Console.WriteLine(ex.Message.ToString());
//                        }
//                    }
//                }
//            }
//        }

//        public static Array SearchGroup(string Name, string userName, string type )
//        {
//            GroupPrincipal qbeGroup = new GroupPrincipal(AdCorrectLink(userName));
//            PrincipalSearcher srch = new PrincipalSearcher(qbeGroup);

//            Array teste = srch.FindAll()
//                .Where(p => p.Name.Contains(Name) && p.StructuralObjectClass == type).Select(p=> p.Name).ToArray();

//            return teste;
//        }    
        

//        public static PrincipalContext AdCorrectLink(string login)
//        {
           
//            var ctx = new PrincipalContext(ContextType.Domain, SystemConstants.LDAP_AMERICAS_PATH_CONFIG);
//            UserPrincipal ad_user = UserPrincipal.FindByIdentity(ctx, login);

//            if (ctx == null)
//            {

//                ctx = new PrincipalContext(ContextType.Domain, SystemConstants.LDAP_ASIA_PATH_CONFIG);
//                if (ctx == null)
//                {
//                    ctx = new PrincipalContext(ContextType.Domain, SystemConstants.LDAP_EUROPE_PATH_CONFIG);
//                    if (ctx != null)
//                    {
                        
//                    }

//                }
               

//            }
           


//            return ctx;
//        }

//        public static async System.Threading.Tasks.Task<string> ProfileImage(string email, string pass, string secondEmail)
//        {
//            string imgDataURL = "";

//            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
//            string COMPLETEURL = "https://outlook.office365.com/ews/exchange.asmx/s/GetUserPhoto?email=" + secondEmail + "&size=HR240x240";
          
//            WebRequest REQUEST = WebRequest.Create(COMPLETEURL);

//            REQUEST.Credentials = new NetworkCredential(email, pass);

//            HttpWebResponse RESPONSE = (HttpWebResponse)(await REQUEST.GetResponseAsync()); 

//            if ((RESPONSE.StatusCode == HttpStatusCode.OK ||
//           RESPONSE.StatusCode == HttpStatusCode.Moved ||
//           RESPONSE.StatusCode == HttpStatusCode.Redirect) &&
//           RESPONSE.ContentType.StartsWith("IMAGE", StringComparison.OrdinalIgnoreCase))
//            {
//                WebClient client = new WebClient();

//                client.Credentials = REQUEST.Credentials;

//                Byte[] pageData = client.DownloadData(COMPLETEURL);

//                string imreBase64Data = Convert.ToBase64String(pageData);
//                imgDataURL = string.Format("data:image/jpg;base64,{0}", imreBase64Data);

//            }



//            return imgDataURL;


//        }

//    }
//}
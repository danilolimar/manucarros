﻿using BusinessLayer.Commom;
using BusinessLayer.Models;
using DataLayer.DbSystem.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BusinessLayer.Helpers
{
    public class SaveFileHelper
    {

        //        public static EnumErrorFile CheckExtension(HttpPostedFileBase file, List<string> extension )
        //        {
        //            var fileName = Path.GetExtension(file.FileName);

        //            int ExtensionValidate = extension.IndexOf(fileName);


        //            if (ExtensionValidate < 0)
        //            {
        //                return EnumErrorFile.DANGER;
        //            }
        //            else
        //            {
        //                return EnumErrorFile.SUCCESS ;
        //            }            
        //        }
        //        public static EnumErrorFile CheckSize(HttpPostedFileBase file, double sizeMax)
        //        {

        //            double size = ConvertUnitOfMeasurement.ConvertBytesToMegabytes(file.ContentLength);

        //            if (size > sizeMax)
        //            {

        //                return EnumErrorFile.DANGER;

        //            }
        //            else
        //            {
        //                return EnumErrorFile.SUCCESS;
        //            }
        //        }
        //        public static EnumErrorFile SaveAsFile(HttpPostedFileBase file,string mapPath, string fileName )
        //        {
        //            DbSystemContext _context = new DbSystemContext();

        //            var directory = (from parameters in _context.EfSystemParameters
        //                             from parametersGroup in _context.EfSystemParametersGroups
        //                             where parameters.Name.ToUpper() == "SHARE" && parametersGroup.Name.ToUpper() == "REPOSITORY_FILE" &&
        //                             parameters.ParameterGroupId == parametersGroup.ParameterGroupId
        //                             select new { Description = parameters.Description }).FirstOrDefault().Description.TrimToUpper() ;

        //            var path = Path.Combine(@""+ directory + "\\" + mapPath + "", fileName);
        //            try
        //            {
        //                file.SaveAs(path);

        //                return EnumErrorFile.SUCCESS;
        //            }
        //            catch (Exception ex )
        //            {
        //                return EnumErrorFile.DANGER;
        //            }



        //        }
        //        public static FileUploadModel GetInfoFile(string folder, string fileName, Guid FileGuid)
        //        {
        //            DbSystemContext _context = new DbSystemContext();

        //            var directory = (from parameters in _context.EfSystemParameters
        //                             from parametersGroup in _context.EfSystemParametersGroups
        //                             where parameters.Name.ToUpper() == "SHARE" && parametersGroup.Name.ToUpper() == "REPOSITORY_FILE" &&
        //                             parameters.ParameterGroupId == parametersGroup.ParameterGroupId
        //                             select new { Description = parameters.Description }).FirstOrDefault().Description.TrimToUpper();

        //            FileInfo file = new FileInfo(@"" + directory +"\\"+ folder + "" + fileName);

        //            FileUploadModel FileUpload = new FileUploadModel();
        //            FileUpload.FileGuid = FileGuid;
        //            FileUpload.NameFile = fileName;
        //            FileUpload.SizeFile = ConvertUnitOfMeasurement.ConvertBytesToMegabytes(file.Length);
        //            FileUpload.Type = file.Extension;
        //            FileUpload.Folder = folder;
        //            FileUpload.Update = DateTime.Now;


        //            return FileUpload;
        //        }

        //        public static void DeleteFile(string mapPath, string fileName)
        //        {
        //            DbSystemContext _context = new DbSystemContext();
        //            var directory = (from parameters in _context.EfSystemParameters
        //                             from parametersGroup in _context.EfSystemParametersGroups
        //                             where parameters.Name.ToUpper() == "SHARE" && parametersGroup.Name.ToUpper() == "REPOSITORY_FILE" &&
        //                             parameters.ParameterGroupId == parametersGroup.ParameterGroupId
        //                             select new { Description = parameters.Description }).FirstOrDefault().Description.TrimToUpper();

        //            var file = Path.Combine(@"" + directory + mapPath + "", fileName);
        //            if (File.Exists(file))
        //                File.Delete(file);
        //        }

        public static byte[] ConvertByArray(Stream input)
        {
            byte[] buffer = new byte[input.Length];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }


        public static string Extension(HttpPostedFileBase file)
        {

            string[] extension = file.FileName.Split('.');


            try
            {
                return extension[extension.Length - 1];
            }
            catch
            {
                return null;
            }
        }

    }
}
﻿using DataLayer.DbSystem.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace BusinessLayer.Helpers
{
    public  class SelectListItemHelper
    {

        // Usado para Criar lista com multiplos valores
      
        
        public static IEnumerable<SelectListItem> GetAllAvailablePermissionsToUser(string userName)
        {
            var _context = new DbSystemContext();
            var permission = _context.EfSystemPermissions.Where(p => p.Active && !p.SystemUserCollection.Any(e => e.Email.Equals(userName, StringComparison.InvariantCultureIgnoreCase)))
                    .Select(x => new SelectListItem
                    {
                        Text = x.Name + " - " + x.Description,
                        Value = x.PermissionId.ToString()
                    }
                            ).AsEnumerable();
            return permission; 
        }

        public static IEnumerable<SelectListItem> GetAllUserPermissions(string userName)
        {
            var _context = new DbSystemContext();
            var permission = _context.EfSystemPermissions.Where(p => p.Active && p.SystemUserCollection.Any(e => e.Email.Equals(userName, StringComparison.InvariantCultureIgnoreCase)))
                    .Select(x => new SelectListItem
                    {
                        Text = x.Name + " - " + x.Description,
                        Value = x.PermissionId.ToString()
                    }
                            ).AsEnumerable();
            return permission;
        }


        public static IEnumerable<SelectListItem> GetServicoes(Guid id)
        {
            var _context = new DbSystemContext();
            return _context.EfSystemServicoes.Where(p => p.Active && p.OperationServicoesId == id)
                    .Select(x => new SelectListItem
                    {
                        Text = x.Name ,
                        Value = x.OperationServicoesId.ToString()
                    }
                            ).AsEnumerable();
        }

        public static IEnumerable<SelectListItem> GetAllUser(Guid id)
        {
            var _context = new DbSystemContext();
            return _context.EfCadastroOficina.Where(p => p.Active && p.OperationCadastroOficinaId == id).FirstOrDefault().SystemUserCollection
                    .Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.UserId.ToString()
                    }
                            ).AsEnumerable();
        }
     
    }
}
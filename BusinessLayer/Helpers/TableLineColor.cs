﻿namespace BusinessLayer.Helpers
{
    public class TableLineColor
    {
        public TableLineColor(string value)
        {

            Value = value;
        }

        public string Value { get; set; }

        public static TableLineColor ADD { get { return new TableLineColor("success"); } }
        public static TableLineColor ERROR { get { return new TableLineColor(""); } }
        public static TableLineColor CHANGE { get { return new TableLineColor("warning"); } }
        public static TableLineColor DELETE { get { return new TableLineColor("danger"); } }
    }
}
﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helpers
{
    public class OrderbyComponentHelper
    {
        /// <summary>
        /// Order Elements dynamic
        /// </summary>
        /// <param name="OrderbyComponent"></param>
        /// string - 1:NCM, 2:IPI, 3:II  is required parameter
        /// <param name="parameter"></param>
        /// Exemple - char = char(':',',')
        /// <returns></returns>
        public static List<OrderbyComponent> _OrderbyComponentHelper(string OrderbyComponent, char[] parameter )
        {
            List<OrderbyComponent> Componentlst = new List<OrderbyComponent>();
            if (parameter.Count() > 1 )
            {
                var Component = OrderbyComponent.Split(parameter[1]);
                int i = 0;
                foreach (var item in Component)
                {
                    var ites = item.Split(parameter[0]);
                    OrderbyComponent Orderby = new OrderbyComponent();
                    Orderby.Sequence = i;
                    Orderby.Component = ites[1];
                    Componentlst.Add(Orderby);
                    i = i + 1;
                }
            }
            else if (parameter.Count() == 1)
            {
                var Component = OrderbyComponent.Split(parameter[1]);
              
                int i = 0;
                foreach (var item in Component)
                {
                    
                    OrderbyComponent Orderby = new OrderbyComponent();
                    Orderby.Sequence = i;
                    Orderby.Component = item;
                    Componentlst.Add(Orderby);
                    i = i + 1;

                }


            }

            return Componentlst.OrderBy(p => p.Sequence).ToList();
        }

        public static string _ConvertStringOrderbyComponentHelper(List<OrderbyComponent> OrderbyComponent, char[] delimitator)
        {
            string hash = "";
            foreach (var item in OrderbyComponent)
            {
                hash = item.Sequence  + delimitator[0].ToString() + item.Component + delimitator[1].ToString() + hash;
            }

            return hash.Remove(hash.Length - 1);
        }
    }
}

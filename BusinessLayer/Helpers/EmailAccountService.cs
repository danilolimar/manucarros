﻿
using AE.Net.Mail;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace BusinessLayer.Helpers
{
    public class SendEmail
    {
        public  static bool SendEmailsConfirmEmail(string to, string txtBody, string Subject = "Informativo SMV", string Url = "")
        {
            try
            {
                string file = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Email.txt");  
               var text= System.IO.File.ReadAllText(file);
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.Subject = Subject;
                string Body = text.Replace("{0}", txtBody).Replace("{1}", Url == "" ? ConfigurationManager.AppSettings["URL"] : Url);
                mail.To.Add(to);
                mail.From = new MailAddress("smvsistemademanutencaoveicular@gmail.com");
                mail.Body = Body;
                mail.IsBodyHtml = true;
                //Instância smtp do servidor, neste caso o gmail.
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential
                ("smvsistemademanutencaoveicular@gmail.com", "@SMV1000");// Login e senha do e-mail.
                smtp.EnableSsl = true;
                smtp.Send(mail);

                return true;
            }
            catch(Exception ex)
            {
                SendEmailsConfirmEmail("danilolima.1@hotmail.com", "Error", ex.Message,  "");
                return false;
            }
        }
    }
    //public class EmailAccountService
    //{
    //    public System.Guid ID { get; set; }
    //    public System.Guid PersonnelKey { get; set; }
    //    public string EmailAddress { get; set; }
    //    public string SenderName { get; set; }
    //    public string SMTPServer { get; set; }
    //    public string SMTPPort { get; set; }
    //    public string SMTPUsername { get; set; }
    //    public string SMTPPassword { get; set; }
    //    public Nullable<bool> IsSMTPssl { get; set; }
    //    public string POPServer { get; set; }
    //    public string IncomingPort { get; set; }
    //    public string POPUsername { get; set; }
    //    public string POPpassword { get; set; }
    //    public Nullable<bool> IsPOPssl { get; set; }
    //    public string Fullname { get; set; }
    //    public string Detail { get; set; }
    //    public byte[] Logo { get; set; }
    //    public string LogoType { get; set; }
    //    public Nullable<bool> IsActive { get; set; }
    //    public Nullable<System.Guid> CompanyKey { get; set; }
    //}

    //public class MailMessege : ObjectWHeaders
    //{
    //    public string subject { get; set; }
    //    public string sender { get; set; }
    //    public string UID { get; set; }
    //    public int MessegeNo { get; set; }
    //    public DateTime sendDate { get; set; }

    //    public string Body { get; set; }
    //    public ICollection<Attachment> Attachments { get; set; }

    //    public string TypeTracker { get; set; }
    //}
}
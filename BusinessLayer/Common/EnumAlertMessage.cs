﻿namespace BusinessLayer.Commom
{
    public class EnumAlertMessage
    {
        public EnumAlertMessage(string value)
        {
            Value = value;
        }

        public string Value { get; set; }

        public static EnumAlertMessage SUCCESS { get { return new EnumAlertMessage("alert-success"); } }
        public static EnumAlertMessage INFO { get { return new EnumAlertMessage("alert-info"); } }
        public static EnumAlertMessage WARNING { get { return new EnumAlertMessage("alert-warning"); } }
        public static EnumAlertMessage DANGER { get { return new EnumAlertMessage("alert-danger"); } }
    }
}
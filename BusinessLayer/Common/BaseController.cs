﻿using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BusinessLayer.Commom
{
    public abstract class BaseController : Controller
    {
        private void ShowAlertMessage(string pMessage, EnumAlertMessage typeMessage)
        {
            TempData["message"] = pMessage;
            TempData["messageType"] = typeMessage.Value;
        }

        protected void HideAlertMessage()
        {
            TempData["message"] = string.Empty;
            TempData["messageType"] = string.Empty;
        }

        protected void ShowSuccessMessage(string pMessage)
        {
            ShowAlertMessage(pMessage, EnumAlertMessage.SUCCESS);
        }

        protected void ShowInfoMessage(string pMessage)
        {
            ShowAlertMessage(pMessage, EnumAlertMessage.INFO);
        }

        protected void ShowWarningMessage(string pMessage)
        {
            ShowAlertMessage(pMessage, EnumAlertMessage.WARNING);
        }

        protected void ShowDangerMessage(string pMessage)
        {
            ShowAlertMessage(pMessage, EnumAlertMessage.DANGER);
        }

        protected void ShowDangerMessage(Exception ex)
        {
            if (ex.InnerException != null)
            {
                ShowAlertMessage(ex.InnerException.InnerException.Message, EnumAlertMessage.DANGER);
            }
            else
            {
                ShowAlertMessage(ex.Message, EnumAlertMessage.DANGER);
            }
        }

        protected void ShowDangerMessage(DbEntityValidationException ex)
        {
            var sb = new StringBuilder();
            foreach (var failure in ex.EntityValidationErrors)
            {
                sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                foreach (var error in failure.ValidationErrors)
                {
                    sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                    sb.AppendLine();
                }
            }
            ShowDangerMessage("Entity Validation Failed - errors follow:\n" + sb.ToString());
        }

        protected void ShowDangerMessage(ModelStateDictionary modelStateDictionary)
        {
            ShowDangerMessage(GetModelStateError(modelStateDictionary));
        }

        protected string GetModelStateError(ModelStateDictionary modelStateDictionary)
        {
            var sb = new StringBuilder();
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    sb.AppendFormat(error.ErrorMessage);
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }

        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
        protected bool RedirectToLocalOficina()
        {
            if (OficinaId() == Guid.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }


           
        }

        protected Guid OficinaId()
        {
            DbSystemContext _context = new DbSystemContext();
            TbOperationCadastroOficina OperationCadastroOficina = Session[SystemConstants.SessionOficina] != null ? 
                (TbOperationCadastroOficina)Session[SystemConstants.SessionOficina] : new TbOperationCadastroOficina();

            var valid = _context.EfCadastroOficina
                        .Where(p => p.OperationCadastroOficinaId == OperationCadastroOficina.OperationCadastroOficinaId)
                        .FirstOrDefault().SystemUserCollection.Where(p => p.User == User.Identity.Name).Count();

            Guid valided = valid > 0 ? OperationCadastroOficina.OperationCadastroOficinaId : Guid.Empty;

            return valided;
        }
    }
}
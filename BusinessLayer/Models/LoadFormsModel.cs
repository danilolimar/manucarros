using BusinessLayer.Commom;
using BusinessLayer.Helpers;
using DataLayer.DbSystem.Context;

using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BusinessLayer.Models
{
     public class LoadFormsModelView                  
    {

        #region forms

        #endregion
 

        public  static List<FileUploadModel> trackerAttachmentUpload(IEnumerable<HttpPostedFileBase> file)
        {
            List<FileUploadModel> lstTbTrackerAttachment = new List<FileUploadModel>();

            foreach (var files in file)
            {
                FileUploadModel trackerAttachment = new FileUploadModel();
   
                trackerAttachment.FileGuid = Guid.NewGuid();
                trackerAttachment.NameFile = files.FileName;
                trackerAttachment.SizeFile = ConvertUnitOfMeasurement.ConvertKilobytesToMegabytes(files.ContentLength);
                trackerAttachment.Type = files.ContentType;
                trackerAttachment.Active = true;
                trackerAttachment.Created = DateTime.Now;
       
 
                var teste = SaveFileHelper.ConvertByArray(files.InputStream);
                trackerAttachment.Base64 = Convert.ToBase64String(teste);
                lstTbTrackerAttachment.Add(trackerAttachment);
            }

            return lstTbTrackerAttachment;
        }


    }
}
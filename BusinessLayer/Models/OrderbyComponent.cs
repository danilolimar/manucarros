﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class OrderbyComponent
    {
        public string Component { get; set; }
        public int Sequence { get; set; }
    }
}

﻿using BusinessLayer.Helpers;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BusinessLayer.Models
{
    public class UsersAll
    {
        public static TbOperationCadastroVeiculos CreateVeiculosValidate(TbOperationCadastroVeiculos tbOperationCadastroVeiculos, Guid idOficina, TbSystemUser systemUserClient)
        {

            DbSystemContext _context = new DbSystemContext();
            TbOperationCadastroVeiculos OperationCadastroVeiculos = _context.EfOperationCadastroVeiculos.Where(p => p.Placa == tbOperationCadastroVeiculos.Placa).FirstOrDefault();
            if (OperationCadastroVeiculos == null)
            {
                tbOperationCadastroVeiculos.OperationCadastroVeiculosId = Guid.NewGuid();
                tbOperationCadastroVeiculos.OperationCadastroOficinaId = idOficina;
                tbOperationCadastroVeiculos.Created = DateTime.Now;
                tbOperationCadastroVeiculos.Modified = DateTime.Now;

                tbOperationCadastroVeiculos.UserId = systemUserClient.UserId;
                _context.EfOperationCadastroVeiculos.Add(tbOperationCadastroVeiculos);
                _context.SaveChanges();
                SendEmail.SendEmailsConfirmEmail(systemUserClient.Email, "Veiculo "+ tbOperationCadastroVeiculos.Marca + " </br> Cadastrado Com Sucesso ", "");
                return tbOperationCadastroVeiculos;
            }
            else
            {
                OperationCadastroVeiculos.OperationCadastroOficina.OperationCadastroOficinaId = idOficina;
              //  _context.EfOperationCadastroVeiculos.Add(OperationCadastroVeiculos);
                _context.Entry(OperationCadastroVeiculos).State = EntityState.Modified;
                _context.SaveChanges();

                return OperationCadastroVeiculos;

            }
        }
        public static TbSystemUser createUser(TbSystemUser tbSystemUser, Guid idOficina)
        {

            DbSystemContext _context = new DbSystemContext();
            TbSystemUser tbSystemUsers = _context.EfSystemUser.Where(p => p.Email == tbSystemUser.Email).FirstOrDefault();

            if (tbSystemUsers == null)
            {
                tbSystemUser.UserId = Guid.NewGuid();
                tbSystemUser.Senha = Guid.NewGuid().ToString();
                tbSystemUser.User = tbSystemUser.Email.Length > 18 ? tbSystemUser.Email.Substring(0, 18) : tbSystemUser.Email;
                tbSystemUser.Telefone = tbSystemUser.Telefone;
                tbSystemUser.CEP = "-";
                tbSystemUser.Endereço = "-";
                tbSystemUser.Active = false;
                tbSystemUser.LastAccess = DateTime.Now;
                tbSystemUser.LastAccess = DateTime.Now;
                tbSystemUser.OficinaCollection.Add(_context.EfCadastroOficina.Where(p => p.OperationCadastroOficinaId == idOficina).FirstOrDefault());
                _context.EfSystemUser.Add(tbSystemUser);
                _context.SaveChanges();
                SendEmail.SendEmailsConfirmEmail(tbSystemUser.Email, "Parabéns, </br> Você foi Cadastrado no SMV e Agora você pode acompanhar as Manutenções no seu carro </br> Utilize este Email "+ tbSystemUser.Email + " e está senha:   " + tbSystemUser.Senha + "</br> Obrigado", "");
                return tbSystemUser;
            }
            else
            {
                TbSystemUser tbSystemUserss = new TbSystemUser();
                tbSystemUserss.UserId = tbSystemUsers.UserId;
                tbSystemUserss.User = tbSystemUsers.User;
                tbSystemUserss.Name = tbSystemUsers.Name;
                tbSystemUserss.Email = tbSystemUsers.Email;
                tbSystemUserss.Telefone = tbSystemUsers.Telefone;
                tbSystemUserss.Endereço = tbSystemUsers.Endereço;

                tbSystemUsers.OficinaCollection.Add(_context.EfCadastroOficina.Where(p => p.OperationCadastroOficinaId == idOficina).FirstOrDefault());
             //   _context.EfSystemUser.Add(tbSystemUsers);
                _context.Entry(tbSystemUsers).State = EntityState.Modified;
                _context.SaveChanges();

               // SendEmail.SendEmailsConfirmEmail(tbSystemUser.Email, "Estamos Enviando este email apenas de agradecimento!", "", Guid.Empty);
                return tbSystemUsers;

            }


        

        }
    }
}
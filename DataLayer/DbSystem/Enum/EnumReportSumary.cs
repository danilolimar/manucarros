﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.DbSystem.Enums
{
    public enum EnumReportSumary
    {
        ACTIVE = 1,
        INACTIVE = 2,
        BOTH = 3
    }
}

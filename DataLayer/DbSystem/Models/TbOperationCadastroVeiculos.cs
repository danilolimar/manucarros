using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbOperationCadastroVeiculos
                    : TEntitySystem
    {
        public TbOperationCadastroVeiculos() {
   
        }

        [Key]
        public Guid OperationCadastroVeiculosId { get; set; }

        [Required]
        public string Marca { get; set; }

        [Required]
        public string Modelo { get; set; }

        [Required]
        public string Placa { get; set; }

        [Required]
        public int Ano { get; set; }

        [Required]
        public string Motor { get; set; }
        [Required]
        public string Potencia { get; set; }

        [Required]
        [UIHint("SimNao")]
        public bool Ar { get; set; }

        [Required]
        [Display(Name = "Direcao Hidraulica")]
        [UIHint("SimNao")]
        public bool DirecaoHidraulica { get; set; }

        [Required]
        [Display(Name = "Km Rodado")]
        public int Km { get; set; }

        [Required]
        [Display(Name = "M�dia Km Rodado Por Mes")]
        public int MediaKmRodadoPorMes { get; set; }

        [UIHint("SimNao")]
        public bool Active { get; set; }


        public Guid UserId { get; set; }

        public virtual TbSystemUser SystemUser { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public Guid OperationCadastroOficinaId { get; set; }

        public virtual TbOperationCadastroOficina OperationCadastroOficina { get; set; }
    }
}
using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbOperationArquivo
                    : TEntitySystem
    {
        public TbOperationArquivo()
        {
        
        }
        [Key]
        public Guid OperationArquivoId { get; set; }
        [Required]
        public string NameFile { get; set; }
        public double SizeFile { get; set; }
        public string Type { get; set; }
        public string Folder { get; set; }
        [UIHint("SimNao")]
        public bool Active { get; set; }
        public DateTime Created { get; set; }
        public string Base64 { get; set; }

        public Guid UserId { get; set; }
        public TbSystemUser SystemUser { get; set; }


        public Guid HistoryVeiculosId { get; set; }
        public virtual TbHistoryVeiculos HistoryVeiculos { get; set; }


    }
}
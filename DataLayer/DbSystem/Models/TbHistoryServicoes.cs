using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.DbSystem.Models
{
    public class TbHistoryServicoes
                    : TEntitySystem
    {
        public TbHistoryServicoes() {
            HistoryVeiculosCollection = new HashSet<TbHistoryVeiculos>();
        }

        [Key]
        public Guid HistoryServicoesId { get; set; }

        public string Description { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0,c}")]
        public decimal Preco { get; set; }

        public virtual ICollection<TbHistoryVeiculos> HistoryVeiculosCollection { get; set; }


    }
}
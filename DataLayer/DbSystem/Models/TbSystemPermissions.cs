using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{ 
    public class TbSystemPermissions : TEntitySystem
    {
        public TbSystemPermissions()
        {
            SystemUserCollection = new HashSet<TbSystemUser>();
        }

        [Key]
        public Guid PermissionId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(300)]
        public string Description { get; set; }

        [UIHint("SimNao")]
        public bool Active { get; set; }

        public virtual ICollection<TbSystemUser> SystemUserCollection { get; set; }
    }
}

using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.DbSystem.Models
{
    public class TbHistoryVeiculos
                    : TEntitySystem
    {
        public TbHistoryVeiculos() {

            OperationArquivoCollection = new HashSet<TbOperationArquivo>();
            HistoryServicoesCollection = new HashSet<TbHistoryServicoes>();
        }

        [Key]
        public Guid HistoryVeiculosId { get; set; }



        [UIHint("SimNao")]
        public bool Active { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        [Required]
        public string Marca { get; set; }

        [Required]
        public string Modelo { get; set; }

        [Required]
        public string Placa { get; set; }

        [Required]
        public int Ano { get; set; }

        [Required]
        public string Motor { get; set; }
        [Required]
        public string Potencia { get; set; }

        [Required]
        public bool Ar { get; set; }

        [Required]
        [Display(Name = "Direcao Hidraulica")]
        public bool DirecaoHidraulica { get; set; }

        [Required]
        [Display(Name = "Km Rodado")]
        public int Km { get; set; }

        [Required]
        public int MediaKmRodadoPorMes { get; set; }

        public virtual ICollection<TbHistoryServicoes> HistoryServicoesCollection { get; set; }


        public Guid OperationCadastroVeiculosId { get; set; }

        public virtual TbOperationCadastroVeiculos OperationCadastroVeiculos { get; set; }

        public Guid UserId { get; set; }

        public virtual TbSystemUser SystemClient { get; set; }

        public virtual ICollection<TbOperationArquivo> OperationArquivoCollection { get; set; }


    }
}
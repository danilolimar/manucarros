using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.DbSystem.Models
{ 
    public class TbOperationOrdemDeServico : TEntitySystem
    {
        public TbOperationOrdemDeServico()
        { }

        [Key]
        public Guid OperationOrdemDeServicoId { get; set; }

        [Required]
        public string OrdemDeServico { get; set; }

        [UIHint("AbertoFinalizado")]
        public bool Active { get; set; }

        public DateTime Created { get; set; }

        public Guid UserId { get; set; }

        public virtual TbSystemUser SystemUser { get; set; }

        [ForeignKey("HistoryVeiculos")]
        public Guid HistoryVeiculosId { get; set; }

        public virtual TbHistoryVeiculos HistoryVeiculos  { get; set; }

        public Guid OperationCadastroOficinaId { get; set; }
        public virtual TbOperationCadastroOficina OperationCadastroOficina { get; set; }


}
}

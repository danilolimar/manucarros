using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbSystemUser : TEntitySystem
    {
        public TbSystemUser()
        {
      
            PermissionsCollection = new HashSet<TbSystemPermissions>();
            CadastroVeiculosCollection = new HashSet<TbOperationCadastroVeiculos>();
            HistoryVeiculosCollection = new HashSet<TbHistoryVeiculos>();
            OficinaCollection = new HashSet<TbOperationCadastroOficina>();
        }

        [Key]
        public Guid UserId { get; set; }

        [Required]
        [StringLength(20)]
        public string User { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

      
        [Required]
        public string Telefone { get; set; }
        //[Required]
        public string CEP { get; set; }

        [Required]
        public string Endere�o { get; set; }
     
        public string Cpf { get; set; }

        public string Senha { get; set; }


        //[Required]
        //public string Slogan { get; set; }

        //[Required]
        //public string Cnpj { get; set; }
        //public bool Funcionario { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        public DateTime LastAccess { get; set; }
        
    #region Other ICollections


        public virtual ICollection<TbSystemPermissions> PermissionsCollection { get; set; }
        public virtual ICollection<TbOperationCadastroVeiculos> CadastroVeiculosCollection { get; set; }

        public virtual ICollection<TbHistoryVeiculos> HistoryVeiculosCollection { get; set; }

        public virtual ICollection<TbOperationCadastroOficina> OficinaCollection { get; set; }
        
        #endregion



    }
}
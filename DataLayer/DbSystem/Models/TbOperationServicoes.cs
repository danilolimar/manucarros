using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbOperationServicoes
                    : TEntitySystem
    {
        public TbOperationServicoes()
        {
            //HistoryVeiculosCollection = new HashSet<TbHistoryVeiculos>();
        }
        [Key]
        public Guid OperationServicoesId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }


        [Required]
        //[StringLength(255)]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0,c}")]
        public decimal Preco { get; set; }

        [UIHint("SimNao")]
        public bool Active { get; set; }

        public Guid UserId { get; set; }

        public TbSystemUser SystemUser { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        //public virtual ICollection<TbHistoryVeiculos> HistoryVeiculosCollection { get; set; }

        //public Guid OperationCadastroOficinaId { get; set; }

        //public virtual TbOperationCadastroOficina OperationCadastroOficina { get; set; }




    }
}
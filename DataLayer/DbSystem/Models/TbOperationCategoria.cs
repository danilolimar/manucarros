using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbOperationCategoria
                    : TEntitySystem
    {
        [Key]
        public Guid OperationCategoriaId { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        public string Descricao { get; set; }

        [UIHint("SimNao")]
        public bool Active { get; set; }

        public TbSystemUser SystemUser { get; set; }

    }
}
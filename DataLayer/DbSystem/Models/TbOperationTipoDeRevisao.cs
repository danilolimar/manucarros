using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbOperationTipoDeRevisao
                    : TEntitySystem
    {


        [Key]
        public Guid OperationTipoDeRevisaoId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        public string Tipo { get; set; }

        
        [UIHint("YESNO")]
        public bool Active { get; set; }

        public TbSystemUser SystemUser { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

    }
}
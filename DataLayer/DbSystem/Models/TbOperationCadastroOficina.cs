using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbOperationCadastroOficina
                    : TEntitySystem
    {

        public TbOperationCadastroOficina()
        {
            SystemUserCollection = new HashSet<TbSystemUser>();
        }
        [Key]
        public Guid OperationCadastroOficinaId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [UIHint("SimNao")]
        public bool Active { get; set; }

        public Guid UserId { get; set; }
        public TbSystemUser SystemUser { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string LogoBase64 { get; set; }
        public string ContentType { get; set; }

        public virtual ICollection<TbSystemUser> SystemUserCollection { get; set; }
    }
}
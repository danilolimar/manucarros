﻿using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.DbSystem.Context
{
    public class DbSystemContext : DbContext
    {
        public DbSystemContext() : 
                base(ConfigurationManager.ConnectionStrings[DatabaseConnectionStrings.DB_SYSTEM_CONTEXT].ToString())
        {
        }
        public DbSystemContext(ConnectionStringSettings connectionStringSettings) : base(connectionStringSettings.ConnectionString)
        {
            this.SetCommandTimeOut(0);
        }


        public void SetCommandTimeOut(int Timeout)
        {
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = Timeout;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            #region ManytoMany

            modelBuilder.Entity<TbSystemUser>()
             .HasMany(e => e.OficinaCollection)
             .WithMany(e => e.SystemUserCollection)
             .Map(m => m.ToTable("TbOperationOficinaTbSystemUsers")
             .MapLeftKey("OperationCadastroOficinaId")
             .MapRightKey("UserId"));


            //modelBuilder.Entity<TbSystemUser>()
            //     .HasMany(e => e.CadastroVeiculosCollection)
            //     .WithMany(e => e.SystemUserCollection)
            //     .Map(m => m.ToTable("TbOperationCadastroVeiculosTbSystemUser")
            //     .MapLeftKey("OperationCadastroVeiculosId")
            //     .MapRightKey("UserId"));

            modelBuilder.Entity<TbSystemUser>()
               .HasMany(e => e.PermissionsCollection)
               .WithMany(e => e.SystemUserCollection)
               .Map(m => m.ToTable("TbSystemPermissionsTbSystemEmployees")
               .MapLeftKey("EmployeeId")
               .MapRightKey("PermissionId"));

            modelBuilder.Entity<TbHistoryVeiculos>()

               .HasMany(e => e.HistoryServicoesCollection)
               .WithMany(e => e.HistoryVeiculosCollection)

               .Map(m => m.ToTable("TbHistoryVeiculosTbHistoryServicoes")
               .MapLeftKey("HistoryVeiculosId")
               .MapRightKey("HistoryServicoesId")

               );


            #endregion

            #region OnetoMany
            //modelBuilder.Entity<TbOperationServicoes>()
            //  .HasMany(a => a.SystemTiposDeServicoesCollection)
            //  .WithRequired(ab => ab.OperationServicoes)
            //    .WillCascadeOnDelete(false);
            #endregion

            #region OnetoOne

            modelBuilder.Entity<TbOperationOrdemDeServico>()
               .HasRequired(s => s.OperationCadastroOficina)
               .WithMany()
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<TbOperationOrdemDeServico>()
              .HasRequired(s => s.SystemUser)
              .WithMany()
              .WillCascadeOnDelete(false);


            //modelBuilder.Entity<TbSystemUser>()
            //    .HasRequired(s => s.CadastroVeiculosCollection)
            //    .WithOptional()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<TbOperationServicoes>()
            // .HasRequired(s => s.SystemUser)
            // .WithOptional()
            // .WillCascadeOnDelete(false);
            #endregion


            modelBuilder.Entity<TbHistoryVeiculos>()
                .HasRequired<TbSystemUser>(s => s.SystemClient)
                .WithMany(g => g.HistoryVeiculosCollection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TbOperationArquivo>()
                 .HasRequired(s => s.HistoryVeiculos)
                 .WithMany(g => g.OperationArquivoCollection)             
                 .WillCascadeOnDelete(false);

            modelBuilder.Entity<TbOperationOrdemDeServico>()
                .HasRequired(a => a.HistoryVeiculos);
                
             //  .WillCascadeOnDelete(false);




        }

        #region modelBilder

        #region System
            public virtual DbSet<TbOperationCadastroOficina> EfCadastroOficina { get; set; }

            public virtual DbSet<TbOperationArquivo> EfOperationArquivo { get; set; }

                public virtual DbSet<TbSystemUser> EfSystemUser { get; set; }
        
                public virtual DbSet<TbSystemPermissions> EfSystemPermissions { get; set; }

                public virtual DbSet<TbHistoryVeiculos> EfHistoryVeiculos { get; set; }

                public virtual DbSet<TbHistoryServicoes> EfHistoryServicoes { get; set; }
 
                public virtual DbSet<TbOperationCadastroVeiculos> EfOperationCadastroVeiculos { get; set; }

                public virtual DbSet<TbOperationServicoes> EfSystemServicoes { get; set; }

                public virtual DbSet<TbOperationOrdemDeServico> EfOperationOrdemDeServico { get; set; }

        #endregion



        #endregion
    }
}

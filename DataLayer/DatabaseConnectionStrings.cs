﻿namespace DataLayer
{
    public static class DatabaseConnectionStrings
    {
        public static readonly string DB_BAAN_CONTEXT = "DB_BAAN_CONTEXT";
        public static readonly string DB_SYSTEM_CONTEXT = "DB_SYSTEM_CONTEXT";
        public static readonly string DB_FLEXFLOW_CONTEXT = "DB_FLEXFLOW_CONTEXT";
        public static readonly string DB_FLEXFLOW_SVC_CONTEXT = "DB_FLEXFLOW_SVC_CONTEXT";
    }
}
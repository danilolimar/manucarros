namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teste1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TbHistoryServicoes", "Description", c => c.String());
            DropColumn("dbo.TbHistoryServicoes", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TbHistoryServicoes", "Name", c => c.String());
            DropColumn("dbo.TbHistoryServicoes", "Description");
        }
    }
}

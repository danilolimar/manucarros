namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teste3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TbHistoryVeiculos", "MediaKmRodadoPorMes", c => c.Int(nullable: false));
            AlterColumn("dbo.TbOperationCadastroVeiculos", "DirecaoHidraulica", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TbOperationCadastroVeiculos", "Km", c => c.Int(nullable: false));
            AlterColumn("dbo.TbHistoryVeiculos", "DirecaoHidraulica", c => c.Boolean(nullable: false));
            AlterColumn("dbo.TbHistoryVeiculos", "Km", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TbHistoryVeiculos", "Km", c => c.String(nullable: false));
            AlterColumn("dbo.TbHistoryVeiculos", "DirecaoHidraulica", c => c.String(nullable: false));
            AlterColumn("dbo.TbOperationCadastroVeiculos", "Km", c => c.String(nullable: false));
            AlterColumn("dbo.TbOperationCadastroVeiculos", "DirecaoHidraulica", c => c.String(nullable: false));
            DropColumn("dbo.TbHistoryVeiculos", "MediaKmRodadoPorMes");
        }
    }
}

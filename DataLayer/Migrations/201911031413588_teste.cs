namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teste : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TbHistoryServicoes",
                c => new
                    {
                        HistoryServicoesId = c.Guid(nullable: false),
                        Name = c.String(),
                        Preco = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryServicoesId);
            
            CreateTable(
                "dbo.TbHistoryVeiculosTbHistoryServicoes",
                c => new
                    {
                        HistoryVeiculosId = c.Guid(nullable: false),
                        HistoryServicoesId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.HistoryVeiculosId, t.HistoryServicoesId })
                .ForeignKey("dbo.TbHistoryVeiculos", t => t.HistoryVeiculosId, cascadeDelete: true)
                .ForeignKey("dbo.TbHistoryServicoes", t => t.HistoryServicoesId, cascadeDelete: true)
                .Index(t => t.HistoryVeiculosId)
                .Index(t => t.HistoryServicoesId);
            
            AlterColumn("dbo.TbOperationServicoes", "Preco", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TbHistoryVeiculosTbHistoryServicoes", "HistoryServicoesId", "dbo.TbHistoryServicoes");
            DropForeignKey("dbo.TbHistoryVeiculosTbHistoryServicoes", "HistoryVeiculosId", "dbo.TbHistoryVeiculos");
            DropIndex("dbo.TbHistoryVeiculosTbHistoryServicoes", new[] { "HistoryServicoesId" });
            DropIndex("dbo.TbHistoryVeiculosTbHistoryServicoes", new[] { "HistoryVeiculosId" });
            AlterColumn("dbo.TbOperationServicoes", "Preco", c => c.Int(nullable: false));
            DropTable("dbo.TbHistoryVeiculosTbHistoryServicoes");
            DropTable("dbo.TbHistoryServicoes");
        }
    }
}

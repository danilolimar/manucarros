namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teste2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TbHistoryServicoes", "Preco", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.TbOperationServicoes", "Preco", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TbOperationServicoes", "Preco", c => c.Double(nullable: false));
            AlterColumn("dbo.TbHistoryServicoes", "Preco", c => c.Double(nullable: false));
        }
    }
}

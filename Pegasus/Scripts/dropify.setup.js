﻿// Basic
$('.dropify').dropify({
	messages: {
		'default': 'Drag and drop a file here or click to select manually',
		'replace': 'Drag and drop or click to replace',
		'remove': 'Remove',
		'error': 'Ooops, something went wrong'
	}})
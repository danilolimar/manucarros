﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pegasus.Models
{
    public class UsersAlls
    {
        [Required]
        [StringLength(20)]

        public string User { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Nome Completo")]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }


        [Required]
        public string Telefone { get; set; }
        [Required]
        public string CEP { get; set; }

        [Required]
        public string Endereço { get; set; }

        //public string? CPF { get; set; }

        [StringLength(100, ErrorMessage = "O/A {0} deve ter no mínimo {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [StringLength(100, ErrorMessage = "O/A {0} deve ter no mínimo {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string RepetirSenha { get; set; }
    }
}
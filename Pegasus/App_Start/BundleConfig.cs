﻿using System.Web;
using System.Web.Optimization;

namespace Pegasus
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                       "~/Scripts/jquery-{version}.js",
                       "~/Scripts/zoomify.js",
                                "~/Scripts/jquery.easing.min.js",
                       "~/Scripts/bootstrap.bundle.js",
                       "~/Scripts/adminlte/fastclick.js",
                         "~/Scripts/adminlte/jquery.slimscroll.js",
         
                         "~/Scripts/jquery.uploadfile.js",
                         "~/Scripts/select2.full.min.js",
                         "~/Scripts/adminlte.js"

                       ));



            bundles.Add(new ScriptBundle("~/bundles/datatable").Include(
                            "~/Scripts/datatable/jquery.datatable.min.js",
                            "~/Scripts/datatable/dataTable.bootstrap4.min.js",
                            //"~/Scripts/datatable/dataTable.buttons.min.js",
                           // "~/Scripts/datatable/buttons.bootstrap4.min.js",
                          //  "~/Scripts/datatable/data-table.js",
                            "~/Scripts/datatable/jszip.min.js",
                            "~/Scripts/datatable/pdfmake.min.js",
                            "~/Scripts/datatable/vfs_fonts.js",
                            "~/Scripts/datatable/buttons.html5.min.js",
                            "~/Scripts/datatable/jquery.datatable.min.js",
                            "~/Scripts/datatable/buttons.print.min.js",
                            "~/Scripts/datatable/buttons.colVis.min.js",
                            "~/Scripts/datatable/datatable.rowGroup.min.js",
                            "~/Scripts/datatable/dataTable.select.min.js",
                            "~/Scripts/datatable/dataTable.fixedHeader.min.js",
                            "~/Scripts/partialViewModal.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",

                      "~/Content/adminlte.css",
                      "~/Content/font-awesome.css",
                       "~/Content/uploadfile.css",
                       "~/Content/select2.css"));

            bundles.Add(new StyleBundle("~/Content/progresstracker").Include(
                      "~/Content/processTracker/progress-tracker.css"));

            bundles.Add(new StyleBundle("~/Content/datatables").Include(
                    "~/Content/datatable/dataTables.bootstrap4.css",
                    "~/Content/datatable/buttons.bootstrap4.css",
                    "~/Content/datatable/select.bootstrap4.css",
                    "~/Content/datatable/fixedHeader.bootstrap4.css"
                 ));

            bundles.Add(new StyleBundle("~/Content/fonts").Include(
                  "~/Content/font-awesome.css",
                  "~/Content/circular-std.css",
                  "~/Content/flag-icon.min.css",
                  "~/Content/materialdesignicons.min.css",
                  "~/Content/simple-line-icons.css",
                  "~/Content/themify-icons.css",
                   "~/Content/weather-icons.min.css"
                  ));

            bundles.Add(new StyleBundle("~/Content/load").Include(
                "~/Content/dropify.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/load").Include(
                 "~/Scripts/dropify.js",
                 "~/Scripts/dropify.setup.js"
                ));
        }
    }
}

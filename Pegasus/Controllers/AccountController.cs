﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;

using BusinessLayer.Security;
using DataLayer.DbSystem.Models;
using DataLayer.DbSystem.Context;
using BusinessLayer.Commom;
using BusinessLayer.Helpers;
using Pegasus.Models;
using System.Data.Entity.Validation;
using System.Configuration;

namespace portal.Controllers
{
    [AllowAnonymous]

    public class AccountController : BaseController
    {
        DbSystemContext _context = new DbSystemContext();
        public ActionResult Index(string returnUrl)
        {
             //  var a = SendEmail.SendEmailsConfirmEmail("", "TXTBODY--------------", "", Guid.Empty);
            //DataTracker.EmailSignalCustomer();

            // var a = Guid.NewGuid();

         //   WeightInformation.WeightInformationCheck();
            return View();
        }

  
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                TbSystemUser usuario = _context.EfSystemUser.Where(a => a.Email == model.User && a.Senha == model.Password && a.Active).FirstOrDefault();

                if (usuario != null)
                {
                    if (usuario.Active)
                    {

                        var Authticket = new FormsAuthenticationTicket(
                                                           1,
                                                           usuario.User,
                                                           DateTime.Now,
                                                           DateTime.Now.AddMinutes(5),
                                                           model.RememberMe,
                                                           string.Join(",", _context.EfSystemPermissions.Select(p => p.Name)),
                                                           FormsAuthentication.FormsCookiePath);
                        var hash = FormsAuthentication.Encrypt(Authticket);
                        var Authcookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash)
                        {
                            Expires = DateTime.Now.AddHours(5)
                        };
                        Response.Cookies.Add(Authcookie);
                        TempData["username"] = usuario.User;
                        Session["displayname"] = usuario.Name;

                        if (returnUrl == null) returnUrl = "~/";
                        Response.Redirect(returnUrl, false);
                    }
                    else
                    {
                        TempData["username"] = usuario.User;
                        TempData["displayname"] = usuario.Name;
                        return RedirectToAction("InactiveAccount", "Account");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect username and/or password");
                }
            }
            return View(model);
        }

     
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Account", null);
        }

        [HttpGet]
        public ActionResult InactiveAccount() => View();    
        [HttpGet]
        public ActionResult AccessDenied() => View();

        [HttpGet]
        public ActionResult Cadastre() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastre(RegisterViewModel registerView)
        {
            TbSystemUser user = _context.EfSystemUser.Where(a => a.Email == registerView.Email).FirstOrDefault();
            if (user == null)
            {
                if (ModelState.IsValid)
                {
                    TbSystemUser systemUser = new TbSystemUser();

                    systemUser.Name = registerView.Nome;
                    systemUser.User = registerView.Email.Substring(0, 18);
                    systemUser.Email = registerView.Email;
                    systemUser.UserId = Guid.NewGuid();
                    systemUser.Telefone = "-";
                    systemUser.CEP = "-";
                    systemUser.Endereço = "-";
                    systemUser.Active = false;
                    systemUser.LastAccess = DateTime.Now;

                    if (registerView.Password != registerView.ConfirmPassword)
                    {

                        ModelState.AddModelError("", "Senha não são iguais");
                        return View();
                    }
                    else
                    {
                        systemUser.Senha = registerView.Password;
                    }

                    _context.EfSystemUser.Add(systemUser);
                    _context.Entry(systemUser).State = System.Data.Entity.EntityState.Added;
                    _context.SaveChanges();
                    //try
                    //{
                    //    _context.SaveChanges();
                    //}
                    //catch (DbEntityValidationException ex)
                    //{
                    //    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    //    {
                    //        foreach (var validationError in entityValidationErrors.ValidationErrors)
                    //        {
                    //            Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                    //        }
                    //    }
                    //}



                    ShowSuccessMessage("Cadastro realizado com sucesso, Aguarde um email de confirmação!");

                    SendEmail.SendEmailsConfirmEmail(registerView.Email, "Abaixo o Link para confirmação do seu Registro",
                        "Confirmação de senha",  ConfigurationManager.AppSettings["URL"] + "/ConfirmEmail/" + systemUser.UserId);

                    return View("Index");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                ShowSuccessMessage("Usuario existente");
                return View();
            }
        }

        [HttpGet]
        public ActionResult ForgotPassword() => View();


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel resetPassword)
        {

            TbSystemUser user = _context.EfSystemUser.Where(a=> a.UserId == resetPassword.Code).FirstOrDefault();

                if (ModelState.IsValid)
                {
                    if (user != null)
                    {
                        if (resetPassword.Password != resetPassword.ConfirmPassword)
                        {

                            ModelState.AddModelError("", "Senha não são iguais");
                            return View(resetPassword);
                        }

                        user.Senha = resetPassword.Password;
                        user.Active = true;
                        _context.EfSystemUser.Add(user);
                        _context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();

                        ShowSuccessMessage("Alterado a senha com sucesso");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ShowSuccessMessage("Usuario Inexistente");
                        return View();
                    }
            }
     
            return View();

        }

        [HttpGet]
        public ActionResult ConfirmEmail(Guid ID)
        {
            var user = _context.EfSystemUser.Where(p=> p.UserId == ID).FirstOrDefault();
            if (user == null)
            {
                ShowSuccessMessage("Usuario Inexistente");
                return RedirectToAction("Index");
            }
            else
            {
                user.Active = true;
                _context.EfSystemUser.Add(user);
                _context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();

                return View();
            }
       
        }



    }
}
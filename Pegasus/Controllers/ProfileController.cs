﻿using BusinessLayer.Commom;
using BusinessLayer.Models;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using Pegasus.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pegasus.Controllers
{
    [Authorize]
    public class ProfileController : BaseController
    {
       
        DbSystemContext _context = new DbSystemContext();
        public ActionResult Index()
        {
            var user = _context.EfSystemUser.Where(P => P.User == User.Identity.Name).FirstOrDefault();
            UsersAlls Userclass = new UsersAlls();
            Userclass.CEP = user.CEP;
            Userclass.Email = user.Email;
            Userclass.Endereço = user.Endereço;
            Userclass.Name = user.Name;
            Userclass.Senha = user.Senha;
            Userclass.Telefone = user.Telefone;
            Userclass.User = user.User;
   
            return View(Userclass);
        }

        [HttpPost]
        public ActionResult Index(UsersAlls users)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _context.EfSystemUser.Where(P => P.User == User.Identity.Name).FirstOrDefault();
                    TbSystemUser Userclass = new TbSystemUser();
                    Userclass.Active = user.Active;
                    Userclass.LastAccess = user.LastAccess;
                    Userclass.UserId = user.UserId;
                    Userclass.CEP = users.CEP;
                    Userclass.Cpf = user.Cpf;
                    Userclass.Email = users.Email;
                    Userclass.Endereço = users.Endereço;
                    Userclass.Name = users.Name;
                    Userclass.Senha = users.Senha;
                    Userclass.Telefone = users.Telefone;
                    Userclass.User = users.User;

                    _context.Entry(User).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
              return View();

          
        }

    }
}
﻿using BusinessLayer.Commom;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pegasus.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
       
        DbSystemContext _context = new DbSystemContext();
        public ActionResult Index()
        {
         
            // if(string.IsNullOrEmpty(Session["displayname"] as string)) 
            // {
            //     return RedirectToAction("Logout", "Account", new { Area = "" });

            // }

            //if( (TbOperationCadastroOficina)Session[SystemConstants.SessionOficina] != null)
            // {
            //     Session[SystemConstants.SessionOficina] = null;
            // }



            var user = _context.EfSystemUser.Where(p => p.User == User.Identity.Name).FirstOrDefault();
            var ordens = _context.EfSystemUser.Where(p => p.User == User.Identity.Name).FirstOrDefault().CadastroVeiculosCollection.ToList();
            List<TbOperationCadastroOficina> listOperationCadastroOficina = new List<TbOperationCadastroOficina>();


            //if (user.PermissionsCollection.Count() > 0)
            //{
            //    if (user.PermissionsCollection.Where(P => P.Name.Contains("SUPORT")).Count() > 0)
            //    {
            //        var Oficina = _context.EfCadastroOficina.Where(p => p.Active && !p.SystemUserCollection.Any(e => e.User.Equals(User.Identity.Name, StringComparison.InvariantCultureIgnoreCase)));

            //        foreach (var item in Oficina)
            //        {
            //            user.OficinaCollection.Add(item);


            //        }
            //        _context.Entry(user).State = EntityState.Modified;
            //        _context.SaveChanges();

            //        return View(user);
            //    }
            //    else if (user.PermissionsCollection.Where(P => P.Name.Contains("ADMIN")).Count() > 0)
            //    {

            //        return View(user);
            //    }
            //    else if (user.PermissionsCollection.Where(P => P.Name.Contains("OFICINA")).Count() > 0)
            //    {
            //        return View(user);
            //    }

            //}

            if (user.PermissionsCollection.Where(P => P.Name.Contains("ADMIN")).Count() == 0 &&
                user.PermissionsCollection.Where(P => P.Name.Contains("SUPORT")).Count() == 0 &&
                user.PermissionsCollection.Where(P => P.Name.Contains("OFICINA")).Count() == 0  )
            {

                return RedirectToAction("Index", "OrdemDeServicoes", new { Area = "Oficina" });
            }
        

            return View(user);
        }
        
        [HttpGet]
        public ActionResult Change(Guid id)
        {
            Session[SystemConstants.SessionOficina] = _context.EfCadastroOficina.Find(id);

             var user = _context.EfSystemUser.Where(p => p.User == User.Identity.Name).FirstOrDefault();   

            if (user.PermissionsCollection.Count() > 0)
            {
                if (user.PermissionsCollection.Where(P => P.Name.Contains("ADMIN")).Count() > 0)
                {

                    return RedirectToAction("Index", "OrdemDeServicoes", new { Area = "Oficina" });
                }
                else if (user.PermissionsCollection.Where(P => P.Name.Contains("SUPORT")).Count() > 0)
                {

                    return RedirectToAction("Index", "OrdemDeServicoes", new { Area = "Oficina" });
                }
                else if (user.PermissionsCollection.Where(P => P.Name.Contains("OFICINA")).Count() > 0)
                {
                    return RedirectToAction("Index", "OrdemDeServicoes", new { Area = "Oficina" });
                }

            }

            return RedirectToAction("Index", "OrdemDeServicoes", new { Area = "Oficina" });
        }


    }
}
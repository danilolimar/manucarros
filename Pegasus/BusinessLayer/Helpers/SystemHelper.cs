﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.OleDb;
using System.IO;
using DataLayer.DbSystem.Context;

namespace BusinessLayer.Helpers
{
    public static class PortalHelpers
    {
        public static bool IsInRole(string pRole)
        {
            return HttpContext.Current.User.IsInRole("SUPPORT") || HttpContext.Current.User.IsInRole(pRole);
        }

        public static MvcHtmlString CheckBoxList(this HtmlHelper htmlHelper, String name, IEnumerable<SelectListItem> selectList, IDictionary<String, Object> htmlAttributes, int rows)
        {
            TagBuilder list = new TagBuilder("table");
            list.MergeAttributes<String, Object>(htmlAttributes);

            var totalRows = rows;
            var totalColumns = Math.Ceiling((double)selectList.Count() / totalRows);
            var index = 0;

            StringBuilder items = new StringBuilder();
            for (int i = 0; i < totalRows; i++)
            {
                index = i;
                items.Append("<tr>");
                for (int j = 0; j < totalColumns; j++)
                {
                    if (index < selectList.Count())
                    {
                        TagBuilder input = new TagBuilder("input");
                        if (selectList.ElementAt(index).Selected) input.MergeAttribute("checked", "checked");
                        input.MergeAttribute("id", selectList.ElementAt(index).Text);
                        input.MergeAttribute("name", name);
                        input.MergeAttribute("type", "checkbox");
                        input.MergeAttribute("value", selectList.ElementAt(index).Value);

                        TagBuilder label = new TagBuilder("label");
                        label.MergeAttribute("for", selectList.ElementAt(index).Text);
                        label.InnerHtml = selectList.ElementAt(index).Text;

                        items.AppendFormat("<td>{0}{1}</td>", input.ToString(TagRenderMode.Normal), label.ToString(TagRenderMode.Normal));
                    }
                    index = index + totalRows;
                }
                items.Append("</tr>");
            }

            list.InnerHtml = items.ToString();

            return MvcHtmlString.Create(list.ToString(TagRenderMode.Normal));
        } // CheckBoxList

        // CheckBoxListFor
        public static MvcHtmlString CheckBoxListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, IDictionary<String, Object> htmlAttributes, int rows = 1) where TModel : class
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            String field = ExpressionHelper.GetExpressionText(expression);

            String name = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);

            if (String.IsNullOrEmpty(name))
                throw new ArgumentException("Name is required", "name");

            selectList = GetSelectList(htmlHelper, name, selectList, true);

            return CheckBoxList(htmlHelper, metadata.DisplayName ?? metadata.PropertyName ?? field, selectList, htmlAttributes == null ? new RouteValueDictionary() : new RouteValueDictionary(htmlAttributes), rows <= 0 ? 1 : rows);
        } // CheckBoxListFor

        // GetSelectData
        private static IEnumerable<SelectListItem> GetSelectData(this HtmlHelper htmlHelper, String name)
        {
            Object data = null;

            if (htmlHelper.ViewData != null)
                data = htmlHelper.ViewData.Eval(name);

            if (data == null)
                throw new InvalidOperationException(String.Format("There is no ViewData item of type '{0}' that has the key '{1}'.", "IEnumerable<SelectListItem>", name));

            IEnumerable<SelectListItem> select = data as IEnumerable<SelectListItem>;
            if (select == null)
                throw new InvalidOperationException(String.Format("The ViewData item that has the key '{0}' is of type '{1}' but must be of type '{2}'.", name, data.GetType().FullName, "IEnumerable<SelectListItem>"));

            return select;
        } // GetSelectData

        // GetSelectList
        private static IEnumerable<SelectListItem> GetSelectList(this HtmlHelper htmlHelper, String name, IEnumerable<SelectListItem> selectList, Boolean allowMultiple)
        {
            Boolean usedViewData = false;

            if (selectList == null)
            {
                selectList = htmlHelper.GetSelectData(name);
                usedViewData = true;
            }

            Object defaultValue = null;
            ModelState modelState;
            if (htmlHelper.ViewData.ModelState.TryGetValue(name, out modelState))
            {
                if (modelState.Value != null)
                {
                    defaultValue = modelState.Value.ConvertTo(allowMultiple ? typeof(String[]) : typeof(String), null);
                }
            }

            if (!usedViewData)
            {
                if (defaultValue == null)
                    defaultValue = htmlHelper.ViewData.Eval(name);
            }

            if (defaultValue != null)
            {
                IEnumerable defaultValues = (allowMultiple) ? defaultValue as IEnumerable : new[] { defaultValue };

                IEnumerable<String> values = from Object value in defaultValues select Convert.ToString(value, CultureInfo.CurrentCulture);

                HashSet<String> selectedValues = new HashSet<string>(values, StringComparer.OrdinalIgnoreCase);

                List<SelectListItem> newSelectList = new List<SelectListItem>();
                foreach (SelectListItem item in selectList)
                {
                    item.Selected = (item.Value != null) ? selectedValues.Contains(item.Value) : selectedValues.Contains(item.Text);
                    newSelectList.Add(item);
                }
                selectList = newSelectList;
            }

            return selectList;
        } // GetSelectList
        

        public static string RefDesCompare(string pre, string current)
        {
            var arrayPre = pre.Split(',').ToList();
            var arrayCurrent = current.Split(',').ToList(); 
            var final = arrayCurrent.Except(arrayPre).ToArray();

            return string.Join(",", final);
        }
    }
}
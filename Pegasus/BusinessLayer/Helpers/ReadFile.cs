﻿using BusinessLayer.Commom;

using DataLayer.DbSystem.Context;
using ExcelDataReader;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;

namespace BusinessLayer.Helpers
{
    public static class ReadFile
    {
        // ExcelEtlXls
        public static DataTable ExcelEtlXls(string local, string sheet = "", string filter = "")
        {
            SystemContext _context = new SystemContext();

            var directory = (from parameters in _context.EfSystemParameters
                             from parametersGroup in _context.EfSystemParametersGroups
                             where parameters.Name.ToUpper() == "SHARE" && parametersGroup.Name.ToUpper() == "REPOSITORY_FILE" &&
                             parameters.ParameterGroupId == parametersGroup.ParameterGroupId
                             select new { Description = parameters.Description }).FirstOrDefault().Description.TrimToUpper();

            var path = Path.Combine(@"" + @directory + "\\" + local);

            var files = (@"" + path + "");

            string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source = " + files +
                       ";Extended Properties=Excel 8.0;";

            DataTable excelDataSet = new DataTable();
            // DataTable dt = null;
            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
               DataTable dbSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                var first = dbSchema.Rows[0].Field<string>("TABLE_NAME");
                string select = "select * from [" + ((sheet + "$").TrimToUpper() == "$" ? first : (sheet + "$")) + "] ";
                OleDbDataAdapter objDa = new OleDbDataAdapter
               (select + filter, conn);
                objDa.Fill(excelDataSet);
            }
            return excelDataSet;

        }
        public static DataTable ExcelEtlXlsx(string local, bool cabecalho = true)
        {
            SystemContext _context = new SystemContext();

            var directory = (from parameters in _context.EfSystemParameters
                             from parametersGroup in _context.EfSystemParametersGroups
                             where parameters.Name.ToUpper() == "SHARE" && parametersGroup.Name.ToUpper() == "REPOSITORY_FILE" &&
                             parameters.ParameterGroupId == parametersGroup.ParameterGroupId
                             select new { Description = parameters.Description }).FirstOrDefault().Description.TrimToUpper();

            var path = Path.Combine(@"" + @directory + "\\" + local);

            var files = (@"" + path + "");
            using (var pck = new OfficeOpenXml.ExcelPackage())
            {
                using (var stream = File.OpenRead(files))
                {
                    pck.Load(stream);
                }
                var ws = pck.Workbook.Worksheets.First();
                DataTable tbl = new DataTable();
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    tbl.Columns.Add(cabecalho ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                var startRow = cabecalho ? 2 : 1;
                for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                }
                return tbl;
            }
        }

 
        public static DataTable ExcelEtlXlsx(Stream File,  int sheet = 0)
        {

            IExcelDataReader excelReader;
            if (File != null)
            {

                try {
                    //excelReader = ExcelDataReader.ExcelReaderFactory.CreateReader(File);
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(File);
                    }
                catch
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(File);
                }

               

                DataSet result = excelReader.AsDataSet();

          
                DataTable dt = result.Tables[0];
                dt.Rows[0].Delete();
                dt.AcceptChanges();
                return dt;
            }

            return null;
        }
    }
}
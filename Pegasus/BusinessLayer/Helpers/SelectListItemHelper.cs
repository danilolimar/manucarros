﻿using DataLayer.DbSystem.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace BusinessLayer.Helpers
{
    public static class SelectListItemHelper
    {

        // Usado para Criar lista com multiplos valores
        //public static IEnumerable<SelectListItem> GetComponentsType()
        //{
            //    var _context = new DbEngUnitOfWork();
            //    return _context.ComponentsTypeRepository.Where(p => p.Active).OrderBy(p => p.Name)
            //            .Select(x => new SelectListItem
            //            {
            //                Text = x.Name ,
            //                Value = x.ComponentsTypeId.ToString()
            //            }
            //                    ).AsEnumerable();
            //}
            //public static IEnumerable<SelectListItem> GetAllActiveLocations()
            //{
            //    var _context = new DbEngUnitOfWork();
            //    return _context.LocationsRepository.Where(p => p.Active).OrderBy(p => p.Name)
            //            .Select(x => new SelectListItem
            //            {
            //                Text = x.Name + " - " + x.Description,
            //                Value = x.LocationId.ToString()
            //            }
            //                    ).AsEnumerable();
            //}

            //public static IEnumerable<SelectListItem> GetAllActiveProducts()
            //{
            //    var _context = new DbEngUnitOfWork();
            //    return _context.ProductsRepository.Where(p => p.Active).OrderBy(p => p.Name)
            //            .Select(x => new SelectListItem
            //            {
            //                Text = x.Name,
            //                Value = x.ProductId.ToString()
            //            }
            //                    ).AsEnumerable();
            //}

            //public static IEnumerable<SelectListItem> GetAllActiveCompanies()
            //{
            //    var _context = new DbEngUnitOfWork();
            //    return _context.CompaniesRepository.Where(p => p.Active).OrderBy(p => p.Code)
            //            .Select(x => new SelectListItem
            //            {
            //                Text = x.Code + " - " + x.Name,
            //                Value = x.CompanyId.ToString()
            //            }
            //                    ).AsEnumerable();
            //}

            //public static IEnumerable<SelectListItem> GetAllActiveEmployees()
            //{
            //    var _context = new DbEngUnitOfWork();
            //    return _context.EmployeesRepository.Where(p => p.Active).OrderBy(p => p.DisplayName)
            //            .Select(x => new SelectListItem
            //            {
            //                Text = x.FlexUser.ToString() + " - " + x.DisplayName,
            //                Value = x.EmployeeId.ToString()
            //            }
            //                    ).AsEnumerable();
            //}

            //public static IEnumerable<SelectListItem> GetAllActiveEmployeesOnlyName()
            //{
            //    var _context = new DbEngUnitOfWork();
            //    return _context.EmployeesRepository.Where(p => p.Active).OrderBy(p => p.DisplayName)
            //            .Select(x => new SelectListItem
            //            {
            //                Text = x.DisplayName,
            //                Value = x.EmployeeId.ToString()
            //            }
            //                    ).AsEnumerable();
            //}

            //public static IEnumerable<SelectListItem> GetAllActiveCompaniesReturningCode()
            //{
            //    var _context = new DbEngUnitOfWork();
            //    return _context.CompaniesRepository.Where(p => p.Active).OrderBy(p => p.Code)
            //            .Select(x => new SelectListItem
            //            {
            //                Text = x.Code + " - " + x.Name,
            //                Value = x.Code
            //            }
            //                    ).AsEnumerable();
            //}

            //public static IEnumerable<SelectListItem> GetAllActiveMeasures()
            //{
            //    var _context = new DbEngUnitOfWork();
            //    return _context.MeasuresRepository.Where(p => p.Active).OrderBy(p => p.Name)
            //            .Select(x => new SelectListItem
            //            {
            //                Text = x.Name.ToString() + " - " + x.Description,
            //                Value = x.MeasureId.ToString()
            //            }).AsEnumerable();
            //}

            public static IEnumerable<SelectListItem> GetAllUserEmailGroups(string userName)
            {
                var _context = new SystemContext();
                return _context.EfSystemEmailGroups.Where(p => p.Active && p.EmployeesCollection.Any(e => e.FlexUser.Equals(userName, StringComparison.InvariantCultureIgnoreCase)))
                        .Select(x => new SelectListItem
                        {
                            Text = x.Name + " - " + x.Description,
                            Value = x.EmailGroupId.ToString()
                        }
                                ).AsEnumerable();
            }

        public static IEnumerable<SelectListItem> GetAllAvailableEmailGroupsToUser(string userName)
        {
            var _context = new SystemContext();
            return _context.EfSystemEmailGroups.Where(p => p.Active && !p.EmployeesCollection.Any(e => e.FlexUser.Equals(userName, StringComparison.InvariantCultureIgnoreCase)))
                    .Select(x => new SelectListItem
                    {
                        Text = x.Name + " - " + x.Description,
                        Value = x.EmailGroupId.ToString()
                    }
                            ).AsEnumerable();
        }

        public static IEnumerable<SelectListItem> GetAllAvailablePermissionsToUser(string userName)
        {
            var _context = new SystemContext();
            return _context.EfSystemPermissions.Where(p => p.Active && !p.EmployeesCollection.Any(e => e.FlexUser.Equals(userName, StringComparison.InvariantCultureIgnoreCase)))
                    .Select(x => new SelectListItem
                    {
                        Text = x.Name + " - " + x.Description,
                        Value = x.PermissionId.ToString()
                    }
                            ).AsEnumerable();
        }

        public static IEnumerable<SelectListItem> GetAllUserPermissions(string userName)
        {
            var _context = new SystemContext();
            return _context.EfSystemPermissions.Where(p => p.Active && p.EmployeesCollection.Any(e => e.FlexUser.Equals(userName, StringComparison.InvariantCultureIgnoreCase)))
                    .Select(x => new SelectListItem
                    {
                        Text = x.Name + " - " + x.Description,
                        Value = x.PermissionId.ToString()
                    }
                            ).AsEnumerable();
        }

        //public static IEnumerable<SelectListItem> GetAllSegmentCodes()
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.SegmentCodesRepository.Where(p => p.Active).OrderBy(p => p.Name)
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Name,
        //                Value = x.SegmentCodeId.ToString()
        //            }
        //                    ).AsEnumerable();
        //}

        //public static IEnumerable<SelectListItem> GetAllFeatures()
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.FeaturesRepository.Where(p => p.Active).OrderBy(p => p.Name)
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Name,
        //                Value = x.FeatureId.ToString()
        //            }
        //                    ).AsEnumerable();
        //}

        //public static IEnumerable<SelectListItem> GetAllResposabilities()
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.ResponsabilitiesRepository.Where(p => p.Active).OrderBy(p => p.Code)
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Code + " - " + x.Description,
        //                Value = x.ResponsabilityId.ToString()
        //            }
        //                    ).AsEnumerable();
        //}

        //public static IEnumerable<SelectListItem> GetAllCustumers()
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.CustomersRepository.Where(p => p.Active).OrderBy(p => p.Name)
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Name,
        //                Value = x.CustomerId.ToString()
        //            }
        //                    ).AsEnumerable();
        //}

        //public static IEnumerable<SelectListItem> GetAllBrands(string SelectedCustomerId = null)
        //{
        //    var _context = new DbEngUnitOfWork();
        //    if (string.IsNullOrEmpty(SelectedCustomerId))
        //    {
        //        return _context.BrandsRepository.Where(p => p.Active).OrderBy(p => p.Name)
        //                .Select(x => new SelectListItem
        //                {
        //                    Text = x.Customer.Name + "-" + x.Name,
        //                    Value = x.BrandId.ToString()
        //                }
        //                        ).AsEnumerable();
        //    }
        //    else
        //    {
        //        return _context.BrandsRepository.Where(p => p.Active && p.CustomerId.ToString().Equals(SelectedCustomerId, StringComparison.InvariantCultureIgnoreCase)).OrderBy(p => p.Name)
        //                .Select(x => new SelectListItem
        //                {
        //                    Text = x.Customer.Name + "-" + x.Name,
        //                    Value = x.BrandId.ToString()
        //                }
        //                        ).AsEnumerable();
        //    }
        //}

        //public static IEnumerable<SelectListItem> GetAllProducts(string SelectedBrandId = null)
        //{
        //    var _context = new DbEngUnitOfWork();
        //    if (string.IsNullOrEmpty(SelectedBrandId))
        //    {
        //        return _context.ProductsRepository.Where(p => p.Active).OrderBy(p => p.Name)
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Brand.Customer.Name + "-" + x.Brand.Name + "-" + x.Name,
        //                Value = x.ProductId.ToString()
        //            }
        //                    ).AsEnumerable();
        //    }
        //    else
        //    {
        //        return _context.ProductsRepository.Where(p => p.Active && p.BrandId.ToString().Equals(SelectedBrandId, StringComparison.InvariantCultureIgnoreCase)).OrderBy(p => p.Name)
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Brand.Customer.Name + "-" + x.Brand.Name + "-" + x.Name,
        //                Value = x.ProductId.ToString()
        //            }
        //                    ).AsEnumerable();
        //    }
        //}

        //public static IEnumerable<SelectListItem> GetAllModels(string SelectedProductId)
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.ModelsRepository.Where(p => p.Active && p.ProductId.ToString().Equals(SelectedProductId, StringComparison.InvariantCultureIgnoreCase)).OrderBy(p => p.Name)
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Name,
        //                Value = x.ProductId.ToString()
        //            }
        //                    ).AsEnumerable();
        //}

        //public static IEnumerable<SelectListItem> GetAllAvailableFeaturesToXcvr(Guid xcvrId)
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.FeaturesRepository.Where(p => p.Active && !p.XcvrsCollection.Any(e => e.XcvrId == xcvrId))
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Name,
        //                Value = x.FeatureId.ToString()
        //            }
        //                    ).AsEnumerable();
        //}

        //public static IEnumerable<SelectListItem> GetAllAvailableAlternatesToXcvr(Guid xcvrId)
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.AlternatesRepository.Where(p => p.Active && !p.XcvrsCollection.Any(e => e.XcvrId == xcvrId))
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Name,
        //                Value = x.AlternateId.ToString()
        //            }
        //                    ).AsEnumerable();
        //}

        //public static IEnumerable<SelectListItem> GetAllAvailableSubBoardsToXcvr(Guid xcvrId)
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.SubBoardsRepository.Where(p => p.Active && !p.XcvrsCollection.Any(e => e.XcvrId == xcvrId))
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Name,
        //                Value = x.SubBoardId.ToString()
        //            }
        //                    ).AsEnumerable();
        //}

        //public static IEnumerable<SelectListItem> GetAllAvailableTrackerGroupToUser(string userName)
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.IssueTrackerJobAnalystRepository.Where(p => p.Active && !p.EmployeesIssueTrackerJobAnalysCollection.Any(e => e.FlexUser.Equals(userName, StringComparison.InvariantCultureIgnoreCase)))
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Group + " - " + x.Responsabilities.Description,
        //                Value = x.JobAnalystGuid.ToString()
        //            }
        //                    ).AsEnumerable();
        //}

        //public static IEnumerable<SelectListItem> GetAllAvailableCompanyWeight()
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.ReportsRepository.Where().Select(p => new { p.Company.Code, p.Company.Name, p.Company.CompanyId }).Distinct()
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.Code + " - " + x.Name,
        //                Value = x.CompanyId.ToString()
        //            }
        //                    ).AsEnumerable();
        //}

        //public static IEnumerable<SelectListItem> GetAllIssueTrackerFutureDatePN(Guid ID)
        //{
        //    var _context = new DbEngUnitOfWork();
        //    return _context.IssueTrackerDeltaRepository.Where(p=> p.Ecoid.EcoGuid == ID).Select(p => new { p.ChildPartNumber}).Distinct()
        //            .Select(x => new SelectListItem
        //            {
        //                Text = x.ChildPartNumber ,
        //                Value = x.ChildPartNumber.ToString()
        //            }
        //                    ).AsEnumerable();
    //}
    }
}
﻿using DataLayer.DbSystem.Context;
using System;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using BusinessLayer.Security;
using DataLayer.DbSystem.Models;
using BusinessLayer.Commom;

namespace BusinessLayer.Security
{
    public static class LdapAuthentication
    {
        
        public static TbSystemEmployees IsAuthenticated(EmployeeInformation _searchResult)
        {
            TbSystemEmployees toReturn = null;
            SystemContext _context = new SystemContext();
           // toReturn = _context.EmployeesRepository.Where(p => p.FlexUser.Equals(_searchResult.SamAccountName, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
            try
            {
                //var _searchResult = GetLdapAuthentication(username, password);
                toReturn = _context.EfSystemEmployees.Where(p => p.FlexUser.Equals(_searchResult.SamAccountName, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();



                if (toReturn == null)
                {
                    toReturn = new TbSystemEmployees();
                    toReturn.EmployeeId = Guid.NewGuid();
                    toReturn.Active = SystemConstants.ACTIVE;
                    toReturn.FlexUser = _searchResult.SamAccountName; // _searchResult.Properties.Contains("uid") ? _searchResult.Properties["uid"][0].TrimToUpper() : _searchResult.Properties["mailnickname"][0].TrimToUpper();
                    toReturn.FlexEmail = _searchResult.mail;
                    toReturn.DisplayName = _searchResult.DisplayName.TrimToUpper();
                    toReturn.FlexSite = _searchResult.Site;
              
                    toReturn.LastAccess = DateTime.Now;
                    _context.EfSystemEmployees.Add(toReturn);
                    _context.SaveChanges();
                }
                else
                {

                    toReturn.LastAccess = DateTime.Now;
                    _context.EfSystemEmployees.Add(toReturn);
                    _context.Entry(toReturn).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                }
                
            }
            catch (Exception ex)
            {
                toReturn = null;
            }
            return toReturn;
            //return new DbEngContext().EfEmployees.Where(p => p.FlexUser.Equals("JAGANSOU")).SingleOrDefault();
        }

      
        public static SearchResult GetLdapAuthenticationInfo(string username, string password)
        {
            var _searchResult = GetLdapAuthentication(SystemConstants.LDAP_AMERICAS_PATH_CONFIG, username, password);
            if (_searchResult == null)
            {
                _searchResult = GetLdapAuthentication(SystemConstants.LDAP_ASIA_PATH_CONFIG, username, password);
            }

            return _searchResult;
        }
        private static SearchResult GetLdapAuthentication(string username, string password)
        {

    
            var _searchResult = GetLdapAuthentication(SystemConstants.LDAP_AMERICAS_PATH_CONFIG, username, password);
            if (_searchResult == null)
            {
                _searchResult = GetLdapAuthentication(SystemConstants.LDAP_ASIA_PATH_CONFIG, username, password);
            }
            return _searchResult;
        }

        private static SearchResult GetLdapAuthentication(string pLdapPathConfig, string pUserName, string pPassword)
        {
            try
            {
                var _directoryEntry = new DirectoryEntry("LDAP://" + pLdapPathConfig, pUserName, pPassword);
                var _directorySearcher = new DirectorySearcher(_directoryEntry);
                _directorySearcher.Filter = "(SAMAccountName=" + pUserName + ")";
                var _searchResult = _directorySearcher.FindOne();
                return _searchResult;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
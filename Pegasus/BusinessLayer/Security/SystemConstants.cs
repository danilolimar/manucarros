﻿using System.Configuration;

namespace BusinessLayer.Security
{
    public class SystemConstants
    {
        public static readonly string LDAP_AMERICAS_PATH_CONFIG = ConfigurationManager.AppSettings["ldapamericaspathconfig"];
        public static readonly string LDAP_EUROPE_PATH_CONFIG = ConfigurationManager.AppSettings["ldapeuropepathconfig"];
        public static readonly string LDAP_ASIA_PATH_CONFIG = ConfigurationManager.AppSettings["ldapasiapathconfig"];
        public static readonly bool ACTIVE = true;
        public static readonly bool INACTIVE = false;
        public static readonly string POPServer = ConfigurationManager.AppSettings["POPServer"];
        public static readonly string POPUsername = ConfigurationManager.AppSettings["POPUsername"];
        public static readonly string POPpassword = "Mito2018";
        public static readonly string IncomingPort = ConfigurationManager.AppSettings["IncomingPort"];
        public static readonly string FORM_FXB = "FXBR";

    }
}
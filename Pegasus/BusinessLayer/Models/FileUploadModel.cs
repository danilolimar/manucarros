﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessLayer.Models
{
    public class FileUploadModel
    {

        public Guid FileGuid { get; set; }
        public string NameFile { get; set; }
        public double SizeFile { get; set; }
        public string Type { get; set; }
        public string Folder { get; set; }
        public DateTime Update { get; set; }

    }
}
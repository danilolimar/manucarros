﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessLayer.Commom
{
    public class EnumErrorFile
    {
        public EnumErrorFile(int value)
        {
            Value = value;
        }

        public int Value { get; set; }

        public static EnumErrorFile SUCCESS { get { return new EnumErrorFile(0); } }

        public static EnumErrorFile DANGER { get { return new EnumErrorFile(1); } }

 
    }
}
﻿using System;

namespace BusinessLayer.Commom
{
    public static class StringExtensions
    {
        public static bool EqualsIgnoreCase(this String source, string value)
        {
            return source.Equals(value, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string TrimToUpper(this string source)
        {
            return string.IsNullOrEmpty(source) ? string.Empty : source.ToString().Trim().ToUpper();
        }

        public static string TrimToUpper(this object source)
        {
            return source == null ? string.Empty : source.ToString().Trim().ToUpper();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Pegasus
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {

            if (HttpContext.Current.User != null)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    if (HttpContext.Current.User.Identity is FormsIdentity)
                    {
                        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;
                        FormsAuthenticationTicket ticket = id.Ticket;
                        //  ticket.Expiration.AddHours(3);
                        // string userData = ticket.UserData;
                        //DbEngContext _db = new DbEngContext();
                        //var userData = _db.EfEmployees.Where(p => p.FlexUser.Equals(User.Identity.Name, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
                        //string[] roles = userData.PermissionsCollection.Select(p => p.Name).ToArray();
                        //HttpContext.Current.User = new GenericPrincipal(id, roles);
                    }
                }
            }
        }
    }

}

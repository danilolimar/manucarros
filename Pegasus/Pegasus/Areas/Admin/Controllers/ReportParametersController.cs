﻿
//using BusinessLayer.Commom;
//using BusinessLayer.Security;
//using DataLayer.DbSystem.Context;
//using System;
//using System.Collections;
//using System.Data;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace portal.Areas.Admin.Controllers
//{
//    [SystemAuthorize(Roles = "ADMIN")]

//    public class ReportParametersController : BaseController
//    {
//        SystemContext _context = new SystemContext();


//        public ReportParametersController()
//        {
//            _context = new SystemContext();
//        }

//        public ActionResult Index()
//        {
//            return View(_context.ReportParametersRepository.Where(p => p.Report.Active).OrderBy(p => p.Report.Name).ThenBy(p => p.Name));
//        }

//        public ActionResult Details(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var tbReportParameters = _dbEngUnitOfWork.ReportParametersRepository.Find(id);
//            if (tbReportParameters == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tbReportParameters);
//        }

//        public ActionResult Edit(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var tbReportParameters = _dbEngUnitOfWork.ReportParametersRepository.Find(id);
//            if (tbReportParameters == null)
//            {
//                return HttpNotFound();
//            }
//            ParameterViewMoldel model = new ParameterViewMoldel();
//            model.ReportParameterId = tbReportParameters.ReportParameterId;
//            model.ReportName = tbReportParameters.Report.Name;
//            model.ParametersName = tbReportParameters.Name;
//            model.ParamtersValues = tbReportParameters.ParameterValues;
//            model.ParamtersNewValues = string.Empty;
//            return View(model);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit(ParameterViewMoldel model)
//        {
//            try
//            {
//                if (string.IsNullOrEmpty(model.ParamtersNewValues))
//                    throw new Exception("New value can not to be empty");

//                var tbReportParameters = _dbEngUnitOfWork.ReportParametersRepository.Find(model.ReportParameterId);
//                var newValues = new ArrayList(model.ParamtersNewValues.TrimToUpper().Split(','));
//                var oldValues = new ArrayList(tbReportParameters.ParameterValues.TrimToUpper().Split(','));
//                foreach (var item in newValues)
//                {
//                    if (!oldValues.Contains(item))
//                    {
//                        oldValues.Add(item);
//                    }
//                }
//                if (oldValues.Count > 0)
//                {
//                    oldValues.Sort();
//                    tbReportParameters.ParameterValues = string.Join(",", oldValues.ToArray());
//                    _dbEngUnitOfWork.Commit();
//                    ShowSuccessMessage("Items added successfully");
//                }
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex);
//            }
//            return RedirectToAction("Index");
//        }

//        public ActionResult Delete(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var tbReportParameters = _dbEngUnitOfWork.ReportParametersRepository.Find(id);
//            if (tbReportParameters == null)
//            {
//                return HttpNotFound();
//            }
//            ParameterViewMoldel model = new ParameterViewMoldel();
//            model.ReportParameterId = tbReportParameters.ReportParameterId;
//            model.ReportName = tbReportParameters.Report.Name;
//            model.ParametersName = tbReportParameters.Name;
//            model.ParamtersValues = tbReportParameters.ParameterValues;
//            model.ParamtersNewValues = string.Empty;
//            return View(model);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Delete(ParameterViewMoldel model)
//        {
//            try
//            {
//                if (string.IsNullOrEmpty(model.ParamtersNewValues))
//                    throw new Exception("New value can not to be empty");

//                var tbReportParameters = _dbEngUnitOfWork.ReportParametersRepository.Find(model.ReportParameterId);
//                var newValues = new ArrayList(model.ParamtersNewValues.TrimToUpper().Split(','));
//                var oldValues = new ArrayList(tbReportParameters.ParameterValues.TrimToUpper().Split(','));
//                foreach (var item in newValues)
//                {
//                    if (oldValues.Contains(item))
//                    {
//                        oldValues.Remove(item);
//                    }
//                }
//                if (oldValues.Count > 0)
//                {
//                    oldValues.Sort();
//                    tbReportParameters.ParameterValues = string.Join(",", oldValues.ToArray());
//                    _dbEngUnitOfWork.Commit();
//                    ShowSuccessMessage("Items successfully removed");
//                }
//                else
//                {
//                    throw new Exception("You can not remove all items, keep at least one");
//                }
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex);
//            }
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                _dbEngUnitOfWork.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}
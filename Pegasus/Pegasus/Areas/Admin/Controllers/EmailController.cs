﻿using BusinessLayer.Commom;
using BusinessLayer.Helpers;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using Pegasus.Areas.Admin.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace portal.Areas.Admin.Controllers
{
    [SystemAuthorize(Roles = "ADMIN")]
    public class EmailController : BaseController
    {
        SystemContext _context = new SystemContext();

        public EmailController()
        {
            _context = new SystemContext();
        }

        // GET: Admin/Email
        public ActionResult Index(string searchString = "")
        {
            var vm = new EmailGroupsByUserViewModel
            {
                AvaliableEmailGroups = SelectListItemHelper.GetAllAvailableEmailGroupsToUser(searchString),
                AppliedEmailGroups = SelectListItemHelper.GetAllUserEmailGroups(searchString),
                currentSearch = searchString,
                userExist = _context.EfSystemEmployees.Where(p => p.FlexUser.Equals(searchString, StringComparison.InvariantCultureIgnoreCase)).Any()
            };
            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_List", vm) : View(vm);
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            string[] itemsAvaliable = form.AllKeys.Contains("SelectedAvaliable") ? form["SelectedAvaliable"].Split(new char[] { ',' }) : new string[0];
            string[] itemsApplied = form.AllKeys.Contains("SelectedApplied") ? form["SelectedApplied"].Split(new char[] { ',' }) : new string[0];
            string userName = form.AllKeys.Contains("currentSearch") ? form["currentSearch"] : null;

            if (userName != null)
            {
                try
                {
                    bool _result = false;
                    var _user = _context.EfSystemEmployees.Where(e => e.FlexUser.Equals(userName, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                    foreach (var item in itemsAvaliable)
                    {
                        var _emailGroup = _context.EfSystemEmailGroups.Where(p => p.EmailGroupId.ToString().Equals(item)).FirstOrDefault();
                        _user.EmailGroupsCollection.Remove(_emailGroup);
                        _context.SaveChanges();
                        _result = true;
                    }
                    foreach (var item in itemsApplied)
                    {
                        var _emailGroup = _context.EfSystemEmailGroups.Where(p => p.EmailGroupId.ToString().Equals(item) && !p.EmployeesCollection.Any(e => e.FlexUser.Equals(userName, StringComparison.InvariantCultureIgnoreCase))).FirstOrDefault();
                        if (_emailGroup != null)
                        {
                            _user.EmailGroupsCollection.Add(_emailGroup);
                            _context.SaveChanges();
                        }
                        _result = true;
                    }
                    if (_result)
                    {
                        ShowSuccessMessage("Successfully applied email group.");
                    }
                }
                catch (Exception ex)
                {
                    ShowDangerMessage(ex.Message);
                }
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();

            }
            base.Dispose(disposing);
        }
    }
}
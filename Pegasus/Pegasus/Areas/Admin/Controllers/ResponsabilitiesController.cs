﻿using BusinessLayer.Commom;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace portal.Areas.Admin.Controllers
{
    [SystemAuthorize(Roles = "ADMIN")]
    public class ResponsabilitiesController : BaseController
    {
        SystemContext _context = new SystemContext();

        public ResponsabilitiesController()
        {
            _context = new SystemContext();
        }

        public ActionResult Index()
        {
            return View(_context.EfSystemResponsabilities);
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbResponsabilities = _context.EfSystemResponsabilities.Find(id);
            if (tbResponsabilities == null)
            {
                return HttpNotFound();
            }
            return View(tbResponsabilities);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TbSystemResponsabilities responsabilities)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var tbResponsabilities = new TbSystemResponsabilities();
                    tbResponsabilities.ResponsabilityId = Guid.NewGuid();
                    tbResponsabilities.Code = responsabilities.Code.TrimToUpper();
                    tbResponsabilities.Description = responsabilities.Description.TrimToUpper();
                    //tbResponsabilities.AgileGroup = responsabilities.AgileGroup.TrimToUpper();
                    tbResponsabilities.Active = responsabilities.Active;
                    tbResponsabilities.Created = DateTime.Now;
                    tbResponsabilities.CreatedEmployee = _context.EfSystemEmployees.Where(p => p.FlexUser.Equals(User.Identity.Name, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
                    _context.EfSystemResponsabilities.Add(tbResponsabilities);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbResponsabilities = _context.EfSystemResponsabilities.Find(id);
            if (tbResponsabilities == null)
            {
                return HttpNotFound();
            }
            return View(tbResponsabilities);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TbSystemResponsabilities tbResponsabilities)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbResponsabilities.Code = tbResponsabilities.Code.TrimToUpper();
                    tbResponsabilities.Description = tbResponsabilities.Description.TrimToUpper();
                    //tbResponsabilities.AgileGroup = tbResponsabilities.AgileGroup.TrimToUpper();
                    _context.EfSystemResponsabilities.Add(tbResponsabilities);
                    _context.Entry(tbResponsabilities).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbResponsabilities = _context.EfSystemResponsabilities.Find(id);
            if (tbResponsabilities == null)
            {
                return HttpNotFound();
            }
            return View(tbResponsabilities);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                var tbResponsabilities = _context.EfSystemResponsabilities.Find(id);
                tbResponsabilities.Active = false;
                _context.EfSystemResponsabilities.Add(tbResponsabilities);
                _context.Entry(tbResponsabilities).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
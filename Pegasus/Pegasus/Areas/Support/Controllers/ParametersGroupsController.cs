﻿using BusinessLayer.Commom;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace portal.Areas.Support.Controllers
{
    [SystemAuthorize(Roles = "ADMIN")]
    public class ParametersGroupsController : BaseController
    {
        SystemContext _context = new SystemContext();

        public ParametersGroupsController()
        {
            _context = new SystemContext();
        }

        // GET: Support/ParametersGroups
        public ActionResult Index()
        {
            return View(_context.EfSystemParametersGroups);
        }

        // GET: Support/ParametersGroups/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbParametersGroup = _context.EfSystemParametersGroups.Find(id);
            if (tbParametersGroup == null)
            {
                return HttpNotFound();
            }
            return View(tbParametersGroup);
        }

        // GET: Support/ParametersGroups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Support/ParametersGroups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TbSystemParametersGroup parametersGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var tbParametersGroup = new TbSystemParametersGroup();
                    tbParametersGroup.ParameterGroupId = Guid.NewGuid();
                    tbParametersGroup.Created = DateTime.Now;
                    tbParametersGroup.Active = parametersGroup.Active;
                    tbParametersGroup.Name = parametersGroup.Name.TrimToUpper();
                    tbParametersGroup.Description = parametersGroup.Description.TrimToUpper();
                    tbParametersGroup.SystemEmployees = _context.EfSystemEmployees.Where(p => p.FlexUser.Equals(User.Identity.Name, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
                    _context.EfSystemParametersGroups.Add(tbParametersGroup);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        // GET: Support/ParametersGroups/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbParametersGroup = _context.EfSystemParametersGroups.Find(id);
            if (tbParametersGroup == null)
            {
                return HttpNotFound();
            }
            return View(tbParametersGroup);
        }

        // POST: Support/ParametersGroups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TbSystemParametersGroup tbParametersGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbParametersGroup.Name = tbParametersGroup.Name.TrimToUpper();
                    tbParametersGroup.Description = tbParametersGroup.Description.TrimToUpper();
                    _context.EfSystemParametersGroups.Add(tbParametersGroup);
                    _context.Entry(tbParametersGroup).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        // GET: Support/ParametersGroups/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbParametersGroup = _context.EfSystemParametersGroups.Find(id);
            if (tbParametersGroup == null)
            {
                return HttpNotFound();
            }
            return View(tbParametersGroup);
        }

        // POST: Support/ParametersGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                var tbParametersGroup = _context.EfSystemParametersGroups.Find(id);
                tbParametersGroup.Active = false;
                _context.EfSystemParametersGroups.Add(tbParametersGroup);
                _context.Entry(tbParametersGroup).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
        
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
﻿//using BusinessLayer.Commom;
//using BusinessLayer.Security;
//using DataLayer.DbSystem.Context;
//using System;
//using System.Net;
//using System.Web.Mvc;

//namespace portal.Areas.Support.Controllers
//{
//    [SystemAuthorize(Roles = "ADMIN")]
//    public class ReportParametersController : BaseController
//    {

//        SystemContext _context = new SystemContext();

//        public ReportParametersController()
//        {
//            _context = new SystemContext();
//        }

//        public ActionResult Index()
//        {
//            return View(_context..Where());
//        }

//        public ActionResult Details(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TbReportParameters tbReportParameters = _dbEngUnitOfWork.ReportParametersRepository.Find(id);
//            if (tbReportParameters == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.ReportId = new SelectList(_dbEngUnitOfWork.ReportsRepository.Where(), "ReportId", "Name");
//            return View(tbReportParameters);
//        }

//        public ActionResult Create()
//        {
//            ViewBag.ReportId = new SelectList(_dbEngUnitOfWork.ReportsRepository.Where(), "ReportId", "Name");
//            return View();
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "ReportParameterId,Name,Sequence,ParameterValues,FieldToLike,ReportParametersType,ReportId")] TbReportParameters tbReportParameters)
//        {
//            try
//            {
//                if (ModelState.IsValid)
//                {
//                    tbReportParameters.ReportParameterId = Guid.NewGuid();
//                    _dbEngUnitOfWork.ReportParametersRepository.Add(tbReportParameters);
//                    _dbEngUnitOfWork.Commit();
//                    ShowSuccessMessage("Create Report Parameter Success");
//                }
//                else
//                {
//                    ShowDangerMessage(GetModelStateError(ModelState));
//                }
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex);
//            }
//            return RedirectToAction("Index");
//        }

//        public ActionResult Edit(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TbReportParameters tbReportParameters = _dbEngUnitOfWork.ReportParametersRepository.Find(id);
//            if (tbReportParameters == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.ReportId = new SelectList(_dbEngUnitOfWork.ReportsRepository.Where(), "ReportId", "Name", tbReportParameters.ReportId);
//            return View(tbReportParameters);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "ReportParameterId,Name,Sequence,ParameterValues,FieldToLike,ReportParametersType,ReportId")] TbReportParameters tbReportParameters)
//        {
//            try
//            {
//                if (ModelState.IsValid)
//                {
//                    _dbEngUnitOfWork.ReportParametersRepository.Update(tbReportParameters);
//                    _dbEngUnitOfWork.Commit();
//                    ShowSuccessMessage("Edit Report Parameter Success");
//                }
//                else
//                {
//                    ShowDangerMessage(GetModelStateError(ModelState));
//                }
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex.InnerException.Message);
//            }
//            return RedirectToAction("Index");
//        }

//        public ActionResult Delete(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TbReportParameters tbReportParameters = _dbEngUnitOfWork.ReportParametersRepository.Find(id);
//            if (tbReportParameters == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.ReportId = new SelectList(_dbEngUnitOfWork.ReportsRepository.Where(), "ReportId", "Name", tbReportParameters.ReportId);
//            return View(tbReportParameters);
//        }

//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(Guid id)
//        {
//            try
//            {
//                TbReportParameters tbReportParameters = _dbEngUnitOfWork.ReportParametersRepository.Find(id);
//                _dbEngUnitOfWork.ReportParametersRepository.Remove(tbReportParameters);
//                _dbEngUnitOfWork.Commit();
//                ShowSuccessMessage("Delete Report Parameter Success");
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex.InnerException.Message);
//            }
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                _dbEngUnitOfWork.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}
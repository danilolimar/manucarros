﻿using BusinessLayer.Commom;
using BusinessLayer.Security;

using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Net;
using System.Web.Mvc;

namespace portal.Areas.Support.Controllers
{
    [SystemAuthorize(Roles = "SUPPORT")]
    public class EmailGroupsController : BaseController
    {

        SystemContext _context = new SystemContext();

        public EmailGroupsController()
        {
            _context = new SystemContext();
        }

        public ActionResult Index()
        {
            return View(_context.EfSystemEmailGroups);
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbEmailGroups = _context.EfSystemEmailGroups.Find(id);
            if (tbEmailGroups == null)
            {
                return HttpNotFound();
            }
            return View(tbEmailGroups);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmailGroupId,Name,Description,Active")] TbSystemEmailGroups tbEmailGroups)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbEmailGroups.EmailGroupId = Guid.NewGuid();
                    _context.EfSystemEmailGroups.Add(tbEmailGroups);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbEmailGroups = _context.EfSystemEmailGroups.Find(id);
            if (tbEmailGroups == null)
            {
                return HttpNotFound();
            }
            return View(tbEmailGroups);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmailGroupId,Name,Description,Active")] TbSystemEmailGroups tbEmailGroups)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.EfSystemEmailGroups.Add(tbEmailGroups);
           
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbEmailGroups = _context.EfSystemEmailGroups.Find(id);
            if (tbEmailGroups == null)
            {
                return HttpNotFound();
            }
            return View(tbEmailGroups);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                var tbEmailGroups = _context.EfSystemEmailGroups.Find(id);

                _context.EfSystemEmailGroups.Add(tbEmailGroups);
                _context.Entry(tbEmailGroups).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
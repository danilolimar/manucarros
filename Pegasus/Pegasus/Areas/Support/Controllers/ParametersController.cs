﻿
using BusinessLayer.Commom;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace portal.Areas.Support.Controllers
{
    [SystemAuthorize(Roles = "SUPPORT")]
    public class ParametersController : BaseController
    {
        SystemContext _context = new SystemContext();

        public ParametersController()
        {
            _context = new SystemContext();
        }

        public ActionResult Index()
        {
            var model = from p in _context.EfSystemParameters orderby p.ParametersGroup.Name, p.Name select p;
            return View(model);
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbParameters = _context.EfSystemParameters.Find(id);
            if (tbParameters == null)
            {
                return HttpNotFound();
            }
            return View(tbParameters);
        }

        public ActionResult Create()
        {
            ViewBag.ParameterGroupId = new SelectList(_context.EfSystemParametersGroups, "ParameterGroupId", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TbSystemParameters tbParameters)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbParameters.ParameterId = Guid.NewGuid();
                    tbParameters.Name = tbParameters.Name.TrimToUpper();
                    tbParameters.ParameterGroupId = tbParameters.ParameterGroupId;
                    tbParameters.SystemEmployees = _context.EfSystemEmployees.Where(p => p.FlexUser.Equals(User.Identity.Name, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
                    tbParameters.Created = DateTime.Now;
                    _context.EfSystemParameters.Add(tbParameters);
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(GetModelStateError(ModelState));
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbParameters = _context.EfSystemParameters.Find(id);
            if (tbParameters == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParameterGroupId = new SelectList(_context.EfSystemParametersGroups, "ParameterGroupId", "Name", tbParameters.ParameterGroupId);
            return View(tbParameters);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TbSystemParameters tbParameters)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbParameters.Name = tbParameters.Name.TrimToUpper();
                    tbParameters.Description = tbParameters.Description.Trim();
                    _context.Entry(tbParameters).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbParameters = _context.EfSystemParameters.Find(id);
            if (tbParameters == null)
            {
                return HttpNotFound();
            }
            return View(tbParameters);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                var tbParameters = _context.EfSystemParameters.Find(id);
                tbParameters.Active = false;
                _context.EfSystemParameters.Add(tbParameters);
                _context.Entry(tbParameters).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
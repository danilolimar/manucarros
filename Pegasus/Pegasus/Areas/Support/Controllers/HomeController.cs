﻿using BusinessLayer.Security;

using System.Web.Mvc;

namespace portal.Areas.Support.Controllers
{
    [SystemAuthorize(Roles = "SUPPORT")]
    public class HomeController : Controller
    {
        // GET: Support/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}
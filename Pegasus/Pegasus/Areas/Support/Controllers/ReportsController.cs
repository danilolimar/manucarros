﻿//using DataLayer.DbEng.Entities;
//using DataLayer.DbEng.UnitOfWork;
//using DataLayer.DbEng.UnitOfWork.Interfaces;
//using portal.Code.Commom;
//using portal.Code.Security;
//using System;
//using System.Net;
//using System.Web.Mvc;

//namespace portal.Areas.Support.Controllers
//{
//    [PortalAuthorize(Roles = "SUPPORT")]
//    public class ReportsController : BaseController
//    {
//        private readonly IDbEngUnitOfWork _dbEngUnitOfWork;

//        public ReportsController()
//        {
//            _dbEngUnitOfWork = new DbEngUnitOfWork();
//        }

//        public ActionResult Index()
//        {
//            return View(_dbEngUnitOfWork.ReportsRepository.Where());
//        }

//        public ActionResult Details(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TbReports tbReports = _dbEngUnitOfWork.ReportsRepository.Find(id);
//            if (tbReports == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.CompanyId = new SelectList(_dbEngUnitOfWork.CompaniesRepository.Where(), "CompanyId", "Name", tbReports.CompanyId);
//            return View(tbReports);
//        }

//        public ActionResult Create()
//        {
//            ViewBag.CompanyId = new SelectList(_dbEngUnitOfWork.CompaniesRepository.Where(), "CompanyId", "Name", Guid.Empty);
//            return View();
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "ReportId,Name,ReportType,DayOfWeek,HourOfDay,SqlQuerie,ParametersCount,Active,CompanyId")] TbReports tbReports)
//        {
//            try
//            {
//                if (ModelState.IsValid)
//                {
//                    tbReports.ReportId = Guid.NewGuid();
//                    _dbEngUnitOfWork.ReportsRepository.Add(tbReports);
//                    _dbEngUnitOfWork.Commit();
//                    ShowSuccessMessage("Create Report Success");
//                }
//                else
//                {
//                    ShowDangerMessage(GetModelStateError(ModelState));
//                }
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex);
//            }
//            return RedirectToAction("Index");
//        }

//        public ActionResult Edit(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TbReports tbReports = _dbEngUnitOfWork.ReportsRepository.Find(id);
//            if (tbReports == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.CompanyId = new SelectList(_dbEngUnitOfWork.CompaniesRepository.Where(), "CompanyId", "Name", tbReports.CompanyId);
//            return View(tbReports);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "ReportId,Name,ReportType,DayOfWeek,HourOfDay,SqlQuerie,ParametersCount,Active,CompanyId")] TbReports tbReports)
//        {
//            try
//            {
//                if (ModelState.IsValid)
//                {
//                    _dbEngUnitOfWork.ReportsRepository.Update(tbReports);
//                    _dbEngUnitOfWork.Commit();
//                    ShowSuccessMessage("Edit Report Success");
//                }
//                else
//                {
//                    ShowDangerMessage(GetModelStateError(ModelState));
//                }
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex.InnerException.Message);
//            }
//            return RedirectToAction("Index");
//        }

//        public ActionResult Delete(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TbReports tbReports = _dbEngUnitOfWork.ReportsRepository.Find(id);
//            if (tbReports == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.CompanyId = new SelectList(_dbEngUnitOfWork.CompaniesRepository.Where(), "CompanyId", "Name", tbReports.CompanyId);
//            return View(tbReports);
//        }

//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(Guid id)
//        {
//            try
//            {
//                TbReports tbReports = _dbEngUnitOfWork.ReportsRepository.Find(id);
//                _dbEngUnitOfWork.ReportsRepository.Remove(tbReports);
//                _dbEngUnitOfWork.Commit();
//                ShowSuccessMessage("Delete Report Success");
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex.InnerException.Message);
//            }
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                _dbEngUnitOfWork.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}
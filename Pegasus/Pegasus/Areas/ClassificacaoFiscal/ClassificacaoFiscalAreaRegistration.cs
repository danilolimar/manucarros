﻿using System.Web.Mvc;

namespace Pegasus.Areas.ClassificacaoFiscal
{
    public class ClassificacaoFiscalAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ClassificacaoFiscal";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ClassificacaoFiscal_default",
                "ClassificacaoFiscal/{controller}/{action}/{id}",
                 new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
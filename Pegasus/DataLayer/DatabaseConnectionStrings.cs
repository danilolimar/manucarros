﻿namespace DataLayer
{
    public static class DatabaseConnectionStrings
    {
        public static readonly string DB_BAAN_CONTEXT = "DB_BAAN_CONTEXT";
        public static readonly string DB_ENG_CONTEXT = "DB_ENG_CONTEXT";
        public static readonly string DB_FLEXFLOW_CONTEXT = "DB_FLEXFLOW_CONTEXT";
        public static readonly string DB_FLEXFLOW_SVC_CONTEXT = "DB_FLEXFLOW_SVC_CONTEXT";
    }
}
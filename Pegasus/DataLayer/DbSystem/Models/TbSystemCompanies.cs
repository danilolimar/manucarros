using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbSystemCompanies: TEntitySystem
    {
        public TbSystemCompanies() {
            ReportSumaryCollection = new HashSet<TbReportSumary>();
        }

        [Key]
        public Guid CompanyId { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        public TbSystemEmployees SystemEmployees { get; set; }

        public ICollection<TbReportSumary> ReportSumaryCollection { get; set; }
        public ICollection<TbJobSumary> JobSumaryCollection { get; set; }
    }
}
using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbJobHistory
                    : TEntitySystem
    {
        public TbJobHistory() {}

        [Key]
        public Guid JobHistoryId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        public TbSystemEmployees SystemEmployees { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

    }
}
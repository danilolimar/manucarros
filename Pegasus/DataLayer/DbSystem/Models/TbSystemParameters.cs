using System;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbSystemParameters : TEntitySystem
    {
        [Key]
        public Guid ParameterId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [Display(Name ="Values")]
        public string Description { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        public DateTime Created { get; set; }

        public Guid ParameterGroupId { get; set; }

        public Guid EmployeeId { get; set; }

        public virtual TbSystemEmployees SystemEmployees { get; set; }

        public virtual TbSystemParametersGroup ParametersGroup { get; set; }
    }
}
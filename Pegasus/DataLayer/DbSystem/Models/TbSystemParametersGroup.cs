using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.DbSystem.Models
{
    [Table("TbParametersGroup")]
    public class TbSystemParametersGroup : TEntitySystem
    {
        public TbSystemParametersGroup()
        {
            ParametersCollection = new HashSet<TbSystemParameters>();
        }

        [Key]
        public Guid ParameterGroupId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(300)]
        public string Description { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        public DateTime Created { get; set; }

        public Guid EmployeeId { get; set; }

        public virtual TbSystemEmployees SystemEmployees { get; set; }

        public virtual ICollection<TbSystemParameters> ParametersCollection { get; set; }
    }
}
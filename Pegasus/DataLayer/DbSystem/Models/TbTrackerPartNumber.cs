using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbTrackerPartNumber
               : TEntitySystem
    {
        public TbTrackerPartNumber() {}

        [Key]
        public Guid TrackerPartNumberId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        public TbSystemEmployees SystemEmployees { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public TbTrackerStatus TrackerStatus { get; set; }

        public TbTrackerAttachment TrackerAttachment { get; set; }

        public TbTrackerComments TrackerComments { get; set; }
    }
}
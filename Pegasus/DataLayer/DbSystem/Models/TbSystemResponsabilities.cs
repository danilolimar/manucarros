using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbSystemResponsabilities : TEntitySystem
    {
        public TbSystemResponsabilities()
        {
            EmployeesCollection = new HashSet<TbSystemEmployees>();
        }

        [Key]
        public Guid ResponsabilityId { get; set; }

        [Required]
        [StringLength(10)]
        public string Code { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }
         
        public DateTime Created { get; set; }

        public Guid CreatedEmployeeId { get; set; }

        public virtual TbSystemEmployees CreatedEmployee { get; set; }

        public virtual ICollection<TbSystemEmployees> EmployeesCollection { get; set; }
    }
}
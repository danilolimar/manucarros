using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbSystemEmailGroups : TEntitySystem
    {
        public TbSystemEmailGroups()
        {
            EmployeesCollection = new HashSet<TbSystemEmployees>();
        }

        [Key]
        public Guid EmailGroupId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(300)]
        public string Description { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        public virtual ICollection<TbSystemEmployees> EmployeesCollection { get; set; }
    }
}
using DataLayer.DbEng.Enums;
using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbJobSumary
                    : TEntitySystem
    {
        public TbJobSumary() {}

        [Key]
        public Guid JobSumaryId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        //public Guid EmployeeId { get; set; }
        public TbSystemEmployees SystemEmployees { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        //public Guid JobTypeId { get; set; }

        public TbJobType JobType { get; set; }

        //public Guid JobParametersId { get; set; }

        public TbJobParameters JobParameters { get; set; }

        public TbSystemCompanies Companies { get; set; }

        public EnumJobSumary Status { get; set; }


    }

 
}
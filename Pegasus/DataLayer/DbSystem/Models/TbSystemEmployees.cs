using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbSystemEmployees : TEntitySystem
    {
        public TbSystemEmployees()
        {
      
            ParametersGroupCollection = new HashSet<TbSystemParametersGroup>();
            ParametersCollection = new HashSet<TbSystemParameters>();

            CreatedResponsabilitiesCollection = new HashSet<TbSystemResponsabilities>();


            EmailGroupsCollection = new HashSet<TbSystemEmailGroups>();

            PermissionsCollection = new HashSet<TbSystemPermissions>();
            ResponsabilitiesCollection = new HashSet<TbSystemResponsabilities>();
    


        }

        [Key]
        public Guid EmployeeId { get; set; }

        [Required]
        [StringLength(20)]
        public string FlexUser { get; set; }

        [Required]
        [StringLength(100)]
        public string FlexEmail { get; set; }

        [Required]
        [StringLength(50)]
        public string FlexSite { get; set; }

        [Required]
        [StringLength(100)]
        public string DisplayName { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        public DateTime LastAccess { get; set; }

        #region Other ICollections
     
        public virtual ICollection<TbSystemParametersGroup> ParametersGroupCollection { get; set; }
        public virtual ICollection<TbSystemParameters> ParametersCollection { get; set; }
       
        public virtual ICollection<TbSystemResponsabilities> CreatedResponsabilitiesCollection { get; set; }
       
        public virtual ICollection<TbSystemEmailGroups> EmailGroupsCollection { get; set; }
     
        public virtual ICollection<TbSystemPermissions> PermissionsCollection { get; set; }
        public virtual ICollection<TbSystemResponsabilities> ResponsabilitiesCollection { get; set; }
        #endregion


 
    }
}
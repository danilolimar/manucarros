using DataLayer.DbEng.Enums;
using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.DbSystem.Models
{
    public class TbReportSumary
                    : TEntitySystem
    {
        public TbReportSumary() {}

        [Key]
        public Guid ReportSumaryId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        public TbSystemEmployees SystemEmployees { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public TbReportQuery ReportQuery { get; set; }

        //public Guid CompanyId { get; set; }
        public virtual TbSystemCompanies Companies { get; set; }

        public EnumReportSumary Status { get; set; }

    }
}
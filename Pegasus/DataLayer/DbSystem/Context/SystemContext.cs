﻿using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.DbSystem.Context
{
    public class SystemContext : DbContext
    {
        public SystemContext() : 
                base("SystemContext")
        {
        }
        public SystemContext(ConnectionStringSettings connectionStringSettings) : base(connectionStringSettings.ConnectionString)
        {
            // this.SetCommandTimeOut(0);
        }
        public void SetCommandTimeOut(int Timeout)
        {
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = Timeout;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            #region ManytoMany
            modelBuilder.Entity<TbSystemEmailGroups>()
               .HasMany(e => e.EmployeesCollection)
               .WithMany(e => e.EmailGroupsCollection)
               .Map(m => m.ToTable("TbSystemEmailGroupsTbSystemEmployees")
               .MapLeftKey("EmailGroupId")
               .MapRightKey("EmployeeId"));


            modelBuilder.Entity<TbSystemEmployees>()
                 .HasMany(e => e.PermissionsCollection)
                 .WithMany(e => e.EmployeesCollection)
                 .Map(m => m.ToTable("TbSystemPermissionsTbSystemEmployees")
                 .MapLeftKey("EmployeeId")
                 .MapRightKey("PermissionId"));

            modelBuilder.Entity<TbSystemEmployees>()
               .HasMany(e => e.ResponsabilitiesCollection)
               .WithMany(e => e.EmployeesCollection)
               .Map(m => m.ToTable("TbSystemResponsabilitiesTbSystemEmployees")
               .MapLeftKey("EmployeeId")
               .MapRightKey("ResponsabilityId"));

            #endregion

            #region OnetoMany

                modelBuilder.Entity<TbSystemEmployees>()
                    .HasMany(e => e.ParametersGroupCollection)
                    .WithRequired(e => e.SystemEmployees)
                    .WillCascadeOnDelete(false);

                modelBuilder.Entity<TbSystemEmployees>()
                    .HasMany(e => e.ParametersCollection)
                    .WithRequired(e => e.SystemEmployees)
                    .WillCascadeOnDelete(false);

                modelBuilder.Entity<TbSystemEmployees>()
                    .HasMany(e => e.CreatedResponsabilitiesCollection)
                    .WithRequired(e => e.CreatedEmployee)
                    .HasForeignKey(e => e.CreatedEmployeeId)
                    .WillCascadeOnDelete(false);

                modelBuilder.Entity<TbSystemParametersGroup>()
                   .HasMany(e => e.ParametersCollection)
                   .WithRequired(e => e.ParametersGroup)
                   .WillCascadeOnDelete(false);

                modelBuilder.Entity<TbSystemCompanies>()
                    .HasMany(e => e.ReportSumaryCollection)
                    .WithRequired(e => e.Companies)
                    .WillCascadeOnDelete(false);

                modelBuilder.Entity<TbSystemCompanies>()
                    .HasMany(e => e.JobSumaryCollection)
                    .WithRequired(e => e.Companies)
                    .WillCascadeOnDelete(false);

            #endregion

        }

        #region modelBilder

            #region System

                public virtual DbSet<TbSystemParameters> EfSystemParameters { get; set; }
                public virtual DbSet<TbSystemParametersGroup> EfSystemParametersGroups { get; set; }
                public virtual DbSet<TbSystemEmployees> EfSystemEmployees { get; set; }
                public virtual DbSet<TbSystemEmailGroups> EfSystemEmailGroups { get; set; }
                public virtual DbSet<TbSystemCompanies> EfSystemCompanies { get; set; }
                public virtual DbSet<TbSystemPermissions> EfSystemPermissions { get; set; }
                public virtual DbSet<TbSystemResponsabilities> EfSystemResponsabilities { get; set; }

            #endregion

            #region Job

                public virtual DbSet<TbJobHistory> EfJobHistory { get; set; }
                public virtual DbSet<TbJobParameters> EfJobParameters { get; set; }
                public virtual DbSet<TbJobSumary> EfJobSumary { get; set; }
                public virtual DbSet<TbJobType> EfJobType { get; set; }

            #endregion

            #region Report

                public virtual DbSet<TbReportQuery> EfReportQuery { get; set; }
                public virtual DbSet<TbReportSumary> EfReportSumary { get; set; }
                public virtual DbSet<TbReportHistory> EfReportHistory { get; set; }

            #endregion

            #region Tracker
                public virtual DbSet<TbTrackerComments> EfTrackerComments { get; set; }
                public virtual DbSet<TbTrackerAttachment> EfTrackerAttachment { get; set; }
                public virtual DbSet<TbTrackerHistory> EfTrackerHistory { get; set; }
                public virtual DbSet<TbTrackerPartNumber> EfTrackerPartNumber { get; set; }
                public virtual DbSet<TbTrackerStatus> EfTrackerStatus { get; set; }
                public virtual DbSet<TbTrackerTicket> EfTrackerTicket { get; set; }

            #endregion

        #endregion
    }
}

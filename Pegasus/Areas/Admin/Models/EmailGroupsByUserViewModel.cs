﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pegasus.Areas.Admin.Models
{
    public class EmailGroupsByUserViewModel
    {
        public string currentSearch { get; set; }
        public bool userExist { get; set; }
        public List<string> SelectedAvaliable { get; set; }
        public List<string> SelectedApplied { get; set; }
        public IEnumerable<SelectListItem> AvaliableEmailGroups { get; set; }
        public IEnumerable<SelectListItem> AppliedEmailGroups { get; set; }
    }
}
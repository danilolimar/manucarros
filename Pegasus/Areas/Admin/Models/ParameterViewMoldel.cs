﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pegasus.Areas.Admin.Models
{
    public class ParameterViewMoldel
    {
        public Guid ReportParameterId { get; set; }
        public string ReportName { get; set; }
        public string ParametersName { get; set; }
        public string ParamtersNewValues { get; set; }
        public string ParamtersValues { get; set; }
    }
}
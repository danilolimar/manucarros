﻿
//using BusinessLayer.Security;
//using DataLayer.DbSystem.Context;
//using DataLayer.DbSystem.Models;
//using System;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Pegasus.Areas.Admin.Controllers
//{
//    [SystemAuthorize(Roles = "ADMIN")]
//    public class EmployeesController : Controller
//    {
//        DbSystemContext _context = new DbSystemContext();


//        public EmployeesController()
//        {
//            _context = new DbSystemContext();
//        }

//        public ActionResult Index()
//        {
//            var efEmployees = _context.EfSystemEmployees.Where(p => !p.PermissionsCollection.Any(e => e.Name.Equals("SUPPORT", StringComparison.InvariantCultureIgnoreCase)));
//            return View(efEmployees.ToList());
//        }

//        public ActionResult Details(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var tbEmployees = _context.EfSystemEmployees.Where(e => e.EmployeeId == id).SingleOrDefault();
//            if (tbEmployees == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tbEmployees);
//        }

//        public ActionResult Create() => View();

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "EmployeeId,FlexUser,FlexEmail,DisplayName,Active")] TbSystemEmployees tbEmployees)
//        {
//            if (ModelState.IsValid)
//            {
//                tbEmployees.EmployeeId = Guid.NewGuid();
//                _context.EfSystemEmployees.Add(tbEmployees);
//                _context.SaveChanges();
//                return RedirectToAction("Index");
//            }

//            return View(tbEmployees);
//        }

//        public ActionResult Edit(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var tbEmployees = _context.EfSystemEmployees.Where(e => e.EmployeeId == id).SingleOrDefault();
//            if (tbEmployees == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tbEmployees);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "EmployeeId,FlexSite,FlexUser,FlexEmail,DisplayName,Active,LastAccess")] TbSystemEmployees tbEmployees)
//        {
//            if (ModelState.IsValid)
//            {
//                _context.EfSystemEmployees.Add(tbEmployees);
//                _context.Entry(tbEmployees).State = System.Data.Entity.EntityState.Modified;
//                _context.SaveChanges();
//                return RedirectToAction("Index");
//            }
//            return View(tbEmployees);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult ChangePermission(Guid EmployeeId, Guid[] Permissions)
//        {
//            if (Permissions.Count() > 0)
//            {
//                var tbEmployees = _context.EfSystemEmployees.Where(e => e.EmployeeId == EmployeeId).SingleOrDefault();
//                foreach (var item in Permissions)
//                {
//                    var _permission = _context.EfSystemPermissions.Find(item);
//                    _permission.EmployeesCollection.Add(tbEmployees);
//                    _context.SaveChanges();
//                }
//            }
//            return RedirectToAction("Edit", new { id = EmployeeId });
//        }

//        public ActionResult RemovePermission(Guid EmployeeId, Guid PermissionId)
//        {
//            var tbEmployees = _context.EfSystemEmployees.Where(e => e.EmployeeId == EmployeeId).SingleOrDefault();
//            var tbPermissions = _context.EfSystemPermissions.Find(PermissionId);
//            tbPermissions.EmployeesCollection.Remove(tbEmployees);
//            _context.SaveChanges();
//            return RedirectToAction("Edit", new { id = EmployeeId });
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult ChangeEmailGroup(Guid EmployeeId, Guid[] EmailGroups)
//        {
//            if (EmailGroups.Count() > 0)
//            {
//                var tbEmployees = _context.EfSystemEmployees.Where(e => e.EmployeeId == EmployeeId).SingleOrDefault();
//                foreach (var item in EmailGroups)
//                {
//                    var _emailGroups = _context.EfSystemEmailGroups.Find(item);
//                    _emailGroups.EmployeesCollection.Add(tbEmployees);
//                    _context.SaveChanges();
//                }
//            }
//            return RedirectToAction("Edit", new { id = EmployeeId });
//        }

//        public ActionResult RemoveEmailGroup(Guid EmployeeId, Guid EmailGroupId)
//        {
//            var tbEmployees = _context.EfSystemEmployees.Where(e => e.EmployeeId == EmployeeId).SingleOrDefault();
//            var tbEmailGroups = _context.EfSystemEmailGroups.Find(EmailGroupId);
//            tbEmailGroups.EmployeesCollection.Remove(tbEmployees);
//            _context.SaveChanges();
//            return RedirectToAction("Edit", new { id = EmployeeId });
//        }



//        //[HttpPost]
//        //[ValidateAntiForgeryToken]
//        //public ActionResult ChangeIssueTrackerGroup(Guid EmployeeId, Guid[] TrackerGroup)
//        //{
//        //    if (TrackerGroup.Count() > 0)
//        //    {
//        //        var tbEmployees = _dbEngUnitOfWork.EmployeesRepository.Where(e => e.EmployeeId == EmployeeId).SingleOrDefault();
//        //        foreach (var item in TrackerGroup)
//        //        {
//        //            var _trackerGroup = _dbEngUnitOfWork.IssueTrackerJobAnalystRepository.Find(item);
//        //            _trackerGroup.EmployeesIssueTrackerJobAnalysCollection.Add(tbEmployees);
//        //            _dbEngUnitOfWork.Commit();
//        //        }
//        //    }
//        //    return RedirectToAction("Edit", new { id = EmployeeId });
//        //}

//        //public ActionResult RemoveIssueTrackerGroup(Guid EmployeeId, Guid TrackerGroupId)
//        //{
//        //    var tbEmployees = _dbEngUnitOfWork.EmployeesRepository.Where(e => e.EmployeeId == EmployeeId).SingleOrDefault();
//        //    var tbTrackerGroup = _dbEngUnitOfWork.IssueTrackerJobAnalystRepository.Find(TrackerGroupId);
//        //    tbTrackerGroup.EmployeesIssueTrackerJobAnalysCollection.Remove(tbEmployees);
//        //    _dbEngUnitOfWork.Commit();
//        //    return RedirectToAction("Edit", new { id = EmployeeId });
//        //}


//        //public ActionResult Delete(Guid? id)
//        //{
//        //    if (id == null)
//        //    {
//        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//        //    }
//        //    var tbEmployees = _dbEngUnitOfWork.EmployeesRepository.Find(id);
//        //    if (tbEmployees == null)
//        //    {
//        //        return HttpNotFound();
//        //    }
//        //    return View(tbEmployees);
//        //}

//        //[HttpPost, ActionName("Delete")]
//        //[ValidateAntiForgeryToken]
//        //public ActionResult DeleteConfirmed(Guid id)
//        //{
//        //    var tbEmployees = _dbEngUnitOfWork.EmployeesRepository.Find(id);
//        //    _dbEngUnitOfWork.EmployeesRepository.Remove(tbEmployees);
//        //    _dbEngUnitOfWork.Commit();
//        //    return RedirectToAction("Index");
//        //}

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                _context.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}
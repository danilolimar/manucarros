﻿
using BusinessLayer.Commom;
using BusinessLayer.Helpers;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using Pegasus.Areas.Admin.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Pegasus.Areas.Admin.Controllers
{
    [SystemAuthorize(Roles = "ADMIN")]
    public class PermissionController : BaseController
    {
        DbSystemContext _context = new DbSystemContext();

        public PermissionController()
        {
            _context = new DbSystemContext();
        }

        // GET: Admin/Permission
        public ActionResult Index(string searchString = "")
        {
            var vm = new PermissionsByUserViewModel
            {
                AvaliablePermissions = SelectListItemHelper.GetAllAvailablePermissionsToUser(searchString),
                AppliedPermissions = SelectListItemHelper.GetAllUserPermissions(searchString),
                currentSearch = searchString,
                userExist = _context.EfSystemUser.Where(P => P.Email == searchString).Any()
            };
            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_List", vm) : View(vm);
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            string[] itemsAvaliable = form.AllKeys.Contains("SelectedAvaliable") ? form["SelectedAvaliable"].Split(new char[] { ',' }) : new string[0];
            string[] itemsApplied = form.AllKeys.Contains("SelectedApplied") ? form["SelectedApplied"].Split(new char[] { ',' }) : new string[0];
            string userName = form.AllKeys.Contains("currentSearch") ? form["currentSearch"] : null;

            if (userName != null)
            {
                try
                {
                    bool _result = false;
                    var _user = _context.EfSystemUser.Where(P => P.Email == userName).FirstOrDefault();
                    foreach (var item in itemsAvaliable)
                    {
                        var _permissions = _context.EfSystemPermissions.Where(p => p.PermissionId.ToString().Equals(item)).FirstOrDefault();
                        _user.PermissionsCollection.Remove(_permissions);
                        _context.SaveChanges();
                        _result = true;
                    }
                    foreach (var item in itemsApplied)
                    {
                        var _permissions = _context.EfSystemPermissions.Where(p => p.PermissionId.ToString().Equals(item)).Where(p => !p.SystemUserCollection
                                .Where(P => P.Email == userName).Any()).FirstOrDefault();
                        if (_permissions != null)
                        {
                            _user.PermissionsCollection.Add(_permissions);
                            _context.SaveChanges();
                        }
                        _result = true;
                    }
                    if (_result)
                    {
                        ShowSuccessMessage("Successfully applied permissions.");
                    }
                }
                catch (Exception ex)
                {
                    ShowDangerMessage(ex.Message);
                }
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
﻿using BusinessLayer.Commom;
using BusinessLayer.Helpers;
using BusinessLayer.Models;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pegasus.Areas.Oficina.Models
{
    public class FormulariodeServicos
    {

        public Guid OperationCadastroVeiculosId { get; set; }
        public TbOperationCadastroVeiculos OperationCadastroVeiculos { get; set; }

        public TbSystemUser SystemUser { get; set; }
        public TbSystemUser SystemUserClient { get; set; }

        public Guid UserId { get; set; }

        public List<FormServicoes> formServicoes { get; set; }
        public List<TbHistoryServicoes> HistoryServicoes { get; set; }

        public TbOperationOrdemDeServico OperationOrdemDeServico { get; set; }

        public List<FormServicoesAttachment> LoadForms { get; set; }

        public IEnumerable<HttpPostedFileBase> FileImage { get; set; }

        public Guid OperationCadastroOficinaId { get; set; }

        public static Guid CreateOrdemService(FormulariodeServicos formulariodeServicos)
        {
            DbSystemContext _context = new DbSystemContext();

            try
            {
                var Veiculo = formulariodeServicos.OperationCadastroVeiculos;//_context.EfOperationCadastroVeiculos.Where(p => p.OperationCadastroVeiculosId == .OperationCadastroOficinaId).FirstOrDefault();

                TbHistoryVeiculos History = new TbHistoryVeiculos();

                History.HistoryVeiculosId = Guid.NewGuid();
                History.OperationCadastroVeiculosId = Veiculo.OperationCadastroVeiculosId;
                History.Active = true;
                History.Created = DateTime.Now;
                History.Modified = DateTime.Now;
                History.UserId = formulariodeServicos.SystemUserClient.UserId ;
                History.Km = Veiculo.Km;
                History.Marca = Veiculo.Marca;
                History.Modelo = Veiculo.Modelo;
                History.Placa = Veiculo.Placa;
                History.Ano = Veiculo.Ano;
                History.Motor = Veiculo.Motor;
                History.Potencia = Veiculo.Potencia;
                History.Ar = Veiculo.Ar;
                History.DirecaoHidraulica = Veiculo.DirecaoHidraulica;
                History.MediaKmRodadoPorMes = Veiculo.MediaKmRodadoPorMes;
                TbOperationOrdemDeServico ordemDeServico = new TbOperationOrdemDeServico();

                var date = DateTime.Now;
                Random randNum = new Random();
                ordemDeServico.OperationOrdemDeServicoId = Guid.NewGuid();
                ordemDeServico.OrdemDeServico = "SMV" + Convert.ToString(date.Date.Day) +
                                                        Convert.ToString(date.Date.Month) +
                                                        Convert.ToString(date.Date.Year) +
                                                        Convert.ToString(date.Date.Minute) +
                                                        Convert.ToString(date.Date.Second) +
                                                        Convert.ToString(date.Date.Hour) +
                                                        Convert.ToString(randNum.Next(100)) +
                                                        Convert.ToString(randNum.Next(101, 900));
                ordemDeServico.Active = true;
                ordemDeServico.Created = DateTime.Now;
                ordemDeServico.UserId = formulariodeServicos.SystemUser.UserId;
                ordemDeServico.OperationCadastroOficinaId = formulariodeServicos.OperationCadastroOficinaId;
                ordemDeServico.HistoryVeiculosId = History.HistoryVeiculosId;

                List<TbOperationArquivo> list = new List<TbOperationArquivo>();
                try
                {
                    foreach (var item in formulariodeServicos.LoadForms)
                    {
                        TbOperationArquivo OperationArquivo = new TbOperationArquivo();

                        OperationArquivo.OperationArquivoId = Guid.NewGuid();
                        OperationArquivo.NameFile = item.NameFile;
                        OperationArquivo.SizeFile = item.SizeFile;
                        OperationArquivo.Type = item.Type;
                        OperationArquivo.Active = true;
                        OperationArquivo.Created = DateTime.Now;
                        OperationArquivo.Base64 = item.Base64;
                        OperationArquivo.UserId = formulariodeServicos.SystemUserClient.UserId;
                        OperationArquivo.HistoryVeiculosId = History.HistoryVeiculosId;
                        list.Add(OperationArquivo);
                    }

                    ordemDeServico.HistoryVeiculosId = History.HistoryVeiculosId;
                    History.OperationArquivoCollection = list;
              
                }
                catch (Exception ex) { }
       

               
                _context.EfOperationOrdemDeServico.Add(ordemDeServico);
                _context.EfOperationArquivo.AddRange(list);
                _context.EfHistoryVeiculos.Add(History);
                _context.SaveChanges();
                
                return ordemDeServico.OperationOrdemDeServicoId;
            }
            catch(Exception ex)
            {
                return Guid.Empty;
            }

           
        }

        public static TbSystemUser CreateAndValidateUser(TbSystemUser tbSystemUser, Guid idOficina)
        {
            DbSystemContext _context = new DbSystemContext();

            var validate = _context.EfSystemUser.Where(p => p.Email == tbSystemUser.Email).FirstOrDefault();

            if(validate == null)
            {
                var user = UsersAll.createUser(tbSystemUser, idOficina);

                return user;
            }
            else
            {
                var user = UsersAll.createUser(validate, idOficina);

                return user;
            }

        }


        public static TbOperationCadastroVeiculos CreateAndValidateVeiculo(TbOperationCadastroVeiculos CadastroVeiculos, Guid idOficina , TbSystemUser systemUserClient)
        {
            DbSystemContext _context = new DbSystemContext();

            var validate = _context.EfOperationCadastroVeiculos.Where(p => p.Placa == CadastroVeiculos.Placa).FirstOrDefault();

            if (validate == null)
            {
                var veiculo = UsersAll.CreateVeiculosValidate(CadastroVeiculos, idOficina, systemUserClient);

                return veiculo;
            }
            else
            {
                var veiculo = UsersAll.CreateVeiculosValidate(validate, idOficina, systemUserClient);

                return veiculo;
            }

        }
    }

    public class FormServicoes
    {
        public Guid OperationTipoDeRevisaoId { get; set; }

        public Guid OperationCategoriaId { get; set; }
    }

    public class FormServicoesAttachment
    {
        public Guid Id { get; set; }

        public string NameFile { get; set; }
        public double SizeFile { get; set; }

        public string Type { get; set; }
        public bool Active { get; set; }

        public string Base64 { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public static List<FormServicoesAttachment> FormServicoesAttachments(IEnumerable<HttpPostedFileBase> file)
        {
            List<FormServicoesAttachment> lstformServicoesAttachment = new List<FormServicoesAttachment>();

            foreach (var files in file)
            {
                FormServicoesAttachment formServicoesAttachment = new FormServicoesAttachment();

                formServicoesAttachment.Id = Guid.NewGuid();
                formServicoesAttachment.NameFile = files.FileName;
                formServicoesAttachment.SizeFile = ConvertUnitOfMeasurement.ConvertKilobytesToMegabytes(files.ContentLength);
                formServicoesAttachment.Type = files.ContentType;
                formServicoesAttachment.Active = true;
                formServicoesAttachment.Created = DateTime.Now;
                formServicoesAttachment.Modified = DateTime.Now;

                var teste = SaveFileHelper.ConvertByArray(files.InputStream);
                formServicoesAttachment.Base64 = Convert.ToBase64String(teste);

                lstformServicoesAttachment.Add(formServicoesAttachment);


            }

            return lstformServicoesAttachment;
        }

        
        public static FormServicoesAttachment FormServicoesAttachments(HttpPostedFileBase file)
        {
            //List<FormServicoesAttachment> lstformServicoesAttachment = new List<FormServicoesAttachment>();

            //foreach (var files in file)
            //{
                FormServicoesAttachment formServicoesAttachment = new FormServicoesAttachment();

                formServicoesAttachment.Id = Guid.NewGuid();
                formServicoesAttachment.NameFile = file.FileName;
                formServicoesAttachment.SizeFile = ConvertUnitOfMeasurement.ConvertKilobytesToMegabytes(file.ContentLength);
                formServicoesAttachment.Type = file.ContentType;
                formServicoesAttachment.Active = true;
                formServicoesAttachment.Created = DateTime.Now;
                formServicoesAttachment.Modified = DateTime.Now;

                var teste = SaveFileHelper.ConvertByArray(file.InputStream);
                formServicoesAttachment.Base64 = Convert.ToBase64String(teste);

               // lstformServicoesAttachment.Add(formServicoesAttachment);


            //}

            return formServicoesAttachment;
        }
    }


}
﻿using DataLayer.DbSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pegasus.Areas.Oficina.Models
{
    public class ServicosModelView
    {
        //public DateTime DataDaSolicitacao { get; set; }
        public TbSystemUser Cliente { get; set; }
        //public TbOperationCadastroVeiculos Veiculo { get; set; }
        public TbOperationOrdemDeServico OrdemDeServico { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0,c}")]
        public decimal PrecoTotal { get; set; }
    }

    public class ServicosAddModelView
    {
        public Guid ServicosId { get; set; }
        public Guid OrdemDeServicoId { get; set; }

        public Guid TipoServicosId { get; set; }

    }
}
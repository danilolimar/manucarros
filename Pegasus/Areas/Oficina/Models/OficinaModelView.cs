﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pegasus.Areas.Oficina.Models
{
    public class OficinaModelView
    {
        [Required]
        [Display(Name = "Logo")]
        public HttpPostedFileBase Files { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [UIHint("YESNO")]
        public bool Active { get; set; }

        public string Base64 { get; set; }

        public Guid OperationCadastroOficinaId { get; set; }
    }
}
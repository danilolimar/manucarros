﻿using System.Web;
using System.Collections.Generic;

namespace Pegasus.Areas.Oficina.Models
{
    public class RequestForms
    {
        const string CHAVE_PEDIDOS = "REQUISICAO";

        internal static void ArmazenaPedidos(FormulariodeServicos produtoDoPedido)
        {
            List<FormulariodeServicos> pedidos = RetornaPedidos() != null ?
                RetornaPedidos() : new List<FormulariodeServicos>();

            pedidos.Add(produtoDoPedido);
            HttpContext.Current.Session[CHAVE_PEDIDOS] = pedidos;
        }

        internal static void RemovePedidos(int id)
        {
            List<FormulariodeServicos> pedidos = RetornaPedidos();
            pedidos.RemoveAt(id);
            HttpContext.Current.Session[CHAVE_PEDIDOS] = pedidos;
        }

        internal static void RemovePedidos()
        {
            HttpContext.Current.Session[CHAVE_PEDIDOS] = new List<FormulariodeServicos>();
        }

        internal static List<FormulariodeServicos> RetornaPedidos()
        {
            return HttpContext.Current.Session[CHAVE_PEDIDOS] != null ?
                (List<FormulariodeServicos>)HttpContext.Current.Session[CHAVE_PEDIDOS] : null;
        }
    }
}
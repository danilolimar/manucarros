﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pegasus.Areas.Oficina.Controllers
{
    public class HomeController : Controller
    {
        // GET: Oficina/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}
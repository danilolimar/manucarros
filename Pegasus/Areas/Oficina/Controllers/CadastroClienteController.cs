﻿
using BusinessLayer.Commom;
using BusinessLayer.Models;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;

namespace Pegasus.Areas.Oficina.Controllers
{
    [SystemAuthorize(Roles = "OFICINA")]
    public class CadastroClienteController : BaseController
    {
        DbSystemContext _context = new DbSystemContext();

        public CadastroClienteController()
        {

        }
        public ActionResult Index()
        {
            Guid idOficina = Guid.Empty;
            try
            {
                idOficina = OficinaId();
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
            if (RedirectToLocalOficina() == false)
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }

            return View(_context.EfCadastroOficina.Find(idOficina).SystemUserCollection.ToList());
        }

     
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TbSystemUser tbSystemUser)
        {
            Guid idOficina = Guid.Empty;
            try
            {
                idOficina = OficinaId();
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
            try
            {

                if (ModelState.IsValid)
                {
                    var user = UsersAll.createUser(tbSystemUser, idOficina);
                 
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfSystemUser.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TbSystemUser tbSystemUser)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Entry(tbSystemUser).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfSystemUser.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                var tbOperationOficina = _context.EfSystemUser.Find(id);
                tbOperationOficina.Active = false;
                _context.EfSystemUser.Add(tbOperationOficina);
                _context.Entry(tbOperationOficina).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
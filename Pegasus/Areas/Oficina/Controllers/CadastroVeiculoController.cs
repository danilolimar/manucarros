﻿
using BusinessLayer.Commom;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;

namespace Pegasus.Areas.Oficina.Controllers
{
    [SystemAuthorize(Roles = "OFICINA")]
    public class CadastroVeiculoController : BaseController
    {
        DbSystemContext _context = new DbSystemContext();

        public CadastroVeiculoController()
        {

        }
        public ActionResult Index()
        {
            try
            {
                if (RedirectToLocalOficina() == false)
                {
                    ShowDangerMessage(SystemConstants.OficinaError);
                    return RedirectToAction("Index", "Home", new { Area = "" });
                }
                Guid id = OficinaId();
                var list = _context.EfOperationCadastroVeiculos.Where(p => p.OperationCadastroOficinaId == id).ToList();
                return View(list);
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
        }


        public ActionResult Create()
        {
            Guid id = OficinaId();
            TbOperationCadastroVeiculos tbOperationCadastro = new TbOperationCadastroVeiculos();
            tbOperationCadastro.OperationCadastroOficinaId = id;
            return View(tbOperationCadastro);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TbOperationCadastroVeiculos tbOperationCadastroVeiculos)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbOperationCadastroVeiculos.OperationCadastroVeiculosId = Guid.NewGuid();
                    tbOperationCadastroVeiculos.OperationCadastroOficinaId = OficinaId();
                    tbOperationCadastroVeiculos.Created = DateTime.Now;
                    tbOperationCadastroVeiculos.Modified = DateTime.Now;
                  
                    tbOperationCadastroVeiculos.UserId = _context.EfSystemUser.Where(p => p.UserId == tbOperationCadastroVeiculos.UserId).FirstOrDefault().UserId;
                    _context.EfOperationCadastroVeiculos.Add(tbOperationCadastroVeiculos);
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfOperationCadastroVeiculos.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TbOperationCadastroVeiculos tbOperationCadastroVeiculos)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Entry(tbOperationCadastroVeiculos).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfOperationCadastroVeiculos.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                var tbOperationCadastroVeiculos = _context.EfOperationCadastroVeiculos.Find(id);
                tbOperationCadastroVeiculos.Active = false;
                _context.EfOperationCadastroVeiculos.Add(tbOperationCadastroVeiculos);
                _context.Entry(tbOperationCadastroVeiculos).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
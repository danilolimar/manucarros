﻿
using BusinessLayer.Commom;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;

namespace Pegasus.Areas.Oficina.Controllers
{
    [SystemAuthorize(Roles = "OFICINA")]
    public class CadastroServicoesController : BaseController
    {
        DbSystemContext _context = new DbSystemContext();

        public CadastroServicoesController()
        {

        }
        public ActionResult Index()
        {
            Guid idOficina = Guid.Empty;
            try
            {
                idOficina = OficinaId();
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
            if (RedirectToLocalOficina() == false)
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }

            return View(_context.EfSystemServicoes.Where(p => p.OperationCadastroOficinaId == idOficina).ToList());
        }

     
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TbOperationServicoes tbOperationServicoes)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbOperationServicoes.OperationServicoesId = Guid.NewGuid();
                    tbOperationServicoes.Created = DateTime.Now;
                    tbOperationServicoes.Modified = DateTime.Now;
                    tbOperationServicoes.OperationCadastroOficinaId = OficinaId();
                    tbOperationServicoes.UserId = _context.EfSystemUser.Where(P => P.User == User.Identity.Name).FirstOrDefault().UserId ;
                    _context.EfSystemServicoes.Add(tbOperationServicoes);
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfSystemServicoes.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TbOperationServicoes tbOperationServicoes)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Entry(tbOperationServicoes).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfHistoryVeiculos.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                var tbServicoes = _context.EfSystemServicoes.Find(id);
                tbServicoes.Active = false;
                _context.EfSystemServicoes.Add(tbServicoes);
                _context.Entry(tbServicoes).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
﻿using BusinessLayer.Commom;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using Pegasus.Areas.Oficina.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Pegasus.Areas.Oficina.Controllers
{
    [Authorize]
    public class OrdemDeServicoesController : BaseController
    {
     
        DbSystemContext _context = new DbSystemContext();
        // GET: Oficina/OrdemDeServicoes
        public ActionResult Index()
        {
            var user = _context.EfSystemUser.Where(p => p.User == User.Identity.Name).FirstOrDefault();

            List<TbOperationOrdemDeServico> listOrdem = new List<TbOperationOrdemDeServico>();
            if (user.PermissionsCollection.Count() > 0)
            {

                Guid idOficina = Guid.Empty;
                    try
                    {
                        idOficina = OficinaId();
                    }
                    catch
                    {
                        ShowDangerMessage(SystemConstants.OficinaError);
                        return RedirectToAction("Index", "Home", new { Area = "" });
                    }

                    if (RedirectToLocalOficina() == false)
                    {
                        ShowDangerMessage(SystemConstants.OficinaError);
                        return RedirectToAction("Index", "Home", new { Area = "" });
                    }

           
                    listOrdem.AddRange(_context.EfOperationOrdemDeServico.Where(p => p.OperationCadastroOficinaId == idOficina 
                    && p.SystemUser.Name == User.Identity.Name && p.Active == false));
                        if (user.PermissionsCollection.Count() > 0)
                        {
                            if (user.PermissionsCollection.Where(P => P.Name.Contains("ADMIN")).Count() > 0)
                            {

                                listOrdem.AddRange(_context.EfOperationOrdemDeServico.Where(p => p.OperationCadastroOficinaId == idOficina));
                                return View(listOrdem);
                            }
                            else if (user.PermissionsCollection.Where(P => P.Name.Contains("SUPORT")).Count() > 0)
                            {
                                listOrdem.AddRange(_context.EfOperationOrdemDeServico);
                                return View(listOrdem);

                            }
                            else if (user.PermissionsCollection.Where(P => P.Name.Contains("OFICINA")).Count() > 0)
                            {
                                listOrdem.AddRange(_context.EfOperationOrdemDeServico.Where(p => p.OperationCadastroOficinaId == idOficina));
                                return View(listOrdem);
                            }
                        }
                return View("Historico", listOrdem);
            }
            else
            {
                listOrdem.AddRange(_context.EfOperationOrdemDeServico.Where(p =>  p.SystemUser.Name == User.Identity.Name));
                return View("HistoricoClient", listOrdem);

            }
        


        
        }

        public ActionResult Servicos(Guid id)
        {
            ServicosModelView servicosModelView = Session["servicos"] != null ? (ServicosModelView)Session["servicos"] : new ServicosModelView();
  
            var ordem = _context.EfOperationOrdemDeServico.Where(p => p.OperationOrdemDeServicoId == id).FirstOrDefault();
            servicosModelView.Cliente = ordem.HistoryVeiculos.SystemClient;
            servicosModelView.PrecoTotal = ordem.HistoryVeiculos.HistoryServicoesCollection.Sum(p => p.Preco);
            servicosModelView.OrdemDeServico = ordem;
            Session["servicos"] = servicosModelView;

            return View(servicosModelView);
        }

        public ActionResult FinalizarPedido(Guid id)
        {
            var ordem = _context.EfOperationOrdemDeServico.Where(p => p.OperationOrdemDeServicoId == id).FirstOrDefault();

            ordem.Active = false;


            _context.Entry(ordem).State = EntityState.Modified;

            _context.SaveChanges();
            ShowSuccessMessage("Ordem Concluida");

            return RedirectToAction("Servicos", new { id = ordem.OperationOrdemDeServicoId }); ;
        }


        //public ActionResult Create()  => View();
        
        //[HttpPost]
        //public ActionResult Create(ServicosAddModelView servicosTipesModel)
        //{
        //    Guid idOficina = Guid.Empty;
        //    try
        //    {
        //        idOficina = OficinaId();
        //    }
        //    catch
        //    {
        //        ShowDangerMessage(SystemConstants.OficinaError);
        //        return RedirectToAction("Index", "Home", new { Area = "" });
        //    }
        //    ServicosModelView servicosModelView = Session["servicos"] != null ? (ServicosModelView)Session["servicos"] : new ServicosModelView();

        //    try
        //    {
             
        //        if (ModelState.IsValid)
        //        {
        //            TbOperationServicoes tbSystemTiposDeServicoes = new TbOperationServicoes();

        //            tbSystemTiposDeServicoes = _context.EfSystemServicoes.Find(servicosTipesModel.ServicosId);
        //            TbHistoryVeiculos tbHistoryVeiculos = new TbHistoryVeiculos();
        //            tbHistoryVeiculos = _context.EfHistoryVeiculos.Where(p => p.HistoryVeiculosId == servicosModelView.OrdemDeServico.HistoryVeiculosId).FirstOrDefault();
        //            tbHistoryVeiculos.SystemServicoesCollection.Add(tbSystemTiposDeServicoes);

        //            _context.Entry(tbHistoryVeiculos).State = EntityState.Modified;

        //            _context.SaveChanges();
        //            Session["servicos"] = null;
        //        }
        //        else
        //        {
        //            ShowDangerMessage(ModelState);
        //        }

        //    }
        //    catch(Exception ex)
        //    {
        //        ShowDangerMessage(ex);
        //    }
        //    return RedirectToAction("Servicos", new { id = servicosModelView.OrdemDeServico.OperationOrdemDeServicoId});
        //}

        //public ActionResult Delete(Guid? id) 
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var tbCompanies = _context.EfSystemTiposDeServicoes.Find(id);
        //    if (tbCompanies == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tbCompanies);
        //}

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(Guid id)
        //{
        //    Guid idOficina = Guid.Empty;
        //    try
        //    {
        //        idOficina = OficinaId();
        //    }
        //    catch
        //    {
        //        ShowDangerMessage(SystemConstants.OficinaError);
        //        return RedirectToAction("Index", "Home", new { Area = "" });
        //    }
        //    ServicosModelView servicosModelView = Session["servicos"] != null ? (ServicosModelView)Session["servicos"] : new ServicosModelView();

        //    try
        //    {

        //        if (ModelState.IsValid)
        //        {
        //            TbOperationServicoes tbSystemTiposDeServicoes = new TbOperationServicoes();

        //            tbSystemTiposDeServicoes = _context.EfSystemServicoes.Find(id);
        //            TbHistoryVeiculos tbHistoryVeiculos = new TbHistoryVeiculos();
        //            tbHistoryVeiculos = _context.EfHistoryVeiculos.Where(p => p.HistoryVeiculosId == servicosModelView.OrdemDeServico.HistoryVeiculosId).FirstOrDefault();
        //            tbHistoryVeiculos.SystemServicoesCollection.Remove(tbSystemTiposDeServicoes);

        //            _context.Entry(tbHistoryVeiculos).State = EntityState.Modified;
        //            _context.SaveChanges();
        //        }
        //        else
        //        {
        //            ShowDangerMessage(ModelState);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ShowDangerMessage(ex);
        //    }
        //    return RedirectToAction("Servicos", new { id = servicosModelView.OrdemDeServico.OperationOrdemDeServicoId });
        //}

     

        public ActionResult SearchServico(string query = "")
        {
            Guid idOficina = Guid.Empty;
            try
            {
                idOficina = OficinaId();
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }

            var users = _context.EfSystemServicoes.Where(p=> p.Active)
                                      
                        .Select(p => new
                        {
                            Id = p.OperationServicoesId,
                            Name = p.Name,
                            Email = ""
                        })
                        .ToList()
                        .Where(m => string.IsNullOrEmpty(query) || m.Name.StartsWith(query))
                        .OrderBy(m => m.Name);
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult SearchTipoServico(Guid id, string query = "")
        //{
        //    Guid idOficina = Guid.Empty;
        //    try
        //    {
        //        idOficina = OficinaId();
        //    }
        //    catch
        //    {
        //        ShowDangerMessage(SystemConstants.OficinaError);
        //        return RedirectToAction("Index", "Home", new { Area = "" });
        //    }

        //    var users = _context.EfSystemTiposDeServicoes
        //                .Where(p => p.OperationCadastroOficinaId == idOficina)
                       
        //                .Select(p => new
        //                {
        //                    Id = p.SystemTiposDeServicoesId,
        //                    Name = p.Name,
        //                    Email = ""
        //                })
        //                .ToList()
        //                .Where(m => string.IsNullOrEmpty(query) || m.Name.StartsWith(query))
        //                .OrderBy(m => m.Name);
        //    return Json(users, JsonRequestBehavior.AllowGet);
        //}

    }
}
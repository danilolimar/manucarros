﻿
using BusinessLayer.Commom;
using BusinessLayer.Helpers;
using BusinessLayer.Models;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using Pegasus.Areas.Oficina.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Pegasus.Areas.Oficina.Controllers
{
    [SystemAuthorize(Roles = "OFICINA")]
    public class CadastroOficinaController : BaseController
    {
        DbSystemContext _context = new DbSystemContext();

        public CadastroOficinaController()
        {

        }
        public ActionResult Index()
        {
            return View(_context.EfCadastroOficina.ToList());
        }


        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OficinaModelView oficinaModelView)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TbOperationCadastroOficina tbOperationCadastroOficina = new TbOperationCadastroOficina();
                    tbOperationCadastroOficina.OperationCadastroOficinaId = Guid.NewGuid();

                    var teste = SaveFileHelper.ConvertByArray(oficinaModelView.Files.InputStream);
                    tbOperationCadastroOficina.Name = oficinaModelView.Name;
                    tbOperationCadastroOficina.LogoBase64 = "data:image/jpeg;base64,"+ Convert.ToBase64String(teste);
                    tbOperationCadastroOficina.Active = oficinaModelView.Active;
                    tbOperationCadastroOficina.ContentType = oficinaModelView.Files.ContentType;
                    tbOperationCadastroOficina.Created = DateTime.Now;
                    tbOperationCadastroOficina.Modified = DateTime.Now;
                    tbOperationCadastroOficina.UserId = _context.EfSystemUser.Where(P => P.User == User.Identity.Name).FirstOrDefault().UserId;
                    _context.EfCadastroOficina.Add(tbOperationCadastroOficina);
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCadastroOficina = _context.EfCadastroOficina.Find(id);
            if (tbCadastroOficina == null)
            {
                return HttpNotFound();
            }
            OficinaModelView oficinaModelView = new OficinaModelView();
            oficinaModelView.Name = tbCadastroOficina.Name;
            oficinaModelView.OperationCadastroOficinaId = tbCadastroOficina.OperationCadastroOficinaId;
            oficinaModelView.Base64 = tbCadastroOficina.LogoBase64;
            oficinaModelView.Active = tbCadastroOficina.Active;



            oficinaModelView.Files = (HttpPostedFileBase)new MemoryPostedFile(Convert.FromBase64String(oficinaModelView.Base64.Replace("data:image/jpeg;base64,","")), "Logo"); 
            return View(oficinaModelView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OficinaModelView oficinaModelView)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TbOperationCadastroOficina tbOperationCadastroOficina = new TbOperationCadastroOficina();
                    var tbCadastroOficina = _context.EfCadastroOficina.Find(oficinaModelView.OperationCadastroOficinaId);
                    tbOperationCadastroOficina.OperationCadastroOficinaId = oficinaModelView.OperationCadastroOficinaId;

                    var teste = SaveFileHelper.ConvertByArray(oficinaModelView.Files.InputStream);
                    tbCadastroOficina.Name = oficinaModelView.Name;
                    tbCadastroOficina.LogoBase64 = "data:image/jpeg;base64," + Convert.ToBase64String(teste);
                    tbCadastroOficina.Active = oficinaModelView.Active;
                    tbCadastroOficina.ContentType = oficinaModelView.Files.ContentType;
                    //tbOperationCadastroOficina.Created = tbCadastroOficina.Created;
                    tbCadastroOficina.Modified = DateTime.Now;
               

                    _context.Entry(tbCadastroOficina).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfCadastroOficina.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                var tbOperationCadastroOficina = _context.EfCadastroOficina.Find(id);
                tbOperationCadastroOficina.Active = false;
                _context.EfCadastroOficina.Add(tbOperationCadastroOficina);
                _context.Entry(tbOperationCadastroOficina).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
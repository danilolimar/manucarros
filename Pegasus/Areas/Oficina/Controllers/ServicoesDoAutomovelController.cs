﻿using BusinessLayer.Commom;
using BusinessLayer.Helpers;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using Pegasus.Areas.Oficina.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Pegasus.Areas.Oficina.Controllers
{
    [SystemAuthorize(Roles = "OFICINA")]
    public class ServicoesDoAutomovelController : BaseController
    {
        
        DbSystemContext _context = new DbSystemContext();
        // GET: Oficina/ServicoesDoAutomovel

        #region LoadOrder
        #region LoadOrder
            public ActionResult SearchUser(string query = "")
            {
                Guid idOficina = Guid.Empty;
                try
                {
                    idOficina = OficinaId();
                }
                catch
                {
                    ShowDangerMessage(SystemConstants.OficinaError);
                    return RedirectToAction("Index", "Home", new { Area = "" });
                }
                FormulariodeServicos carrinho = Session["Carrinho"] != null ? (FormulariodeServicos)Session["Carrinho"] : new FormulariodeServicos();

                var users = _context.EfOperationCadastroVeiculos
                            .Where(p => p.OperationCadastroOficinaId == idOficina )
                            .Where(p=> p.OperationCadastroVeiculosId == carrinho.OperationCadastroVeiculosId)
           
                            .Select(p => new
                            {
                                Id = p.SystemUser.UserId,
                                Name = p.SystemUser.Name,
                                Email = p.SystemUser.Email
                            })
                            .ToList()
                            .Where(m => string.IsNullOrEmpty(query) || m.Name.StartsWith(query))
                            .OrderBy(m => m.Name); 
                return Json(users, JsonRequestBehavior.AllowGet);
            }
            public ActionResult SearchCar( string query = "")
            {
                Guid idOficina = Guid.Empty;
                try
                {
                    idOficina = OficinaId();
                }
                catch
                {
                    ShowDangerMessage(SystemConstants.OficinaError);
                    return RedirectToAction("Index", "Home", new { Area = "" });
                }

                var users = _context.EfOperationCadastroVeiculos
                            .Where(p => p.OperationCadastroOficinaId == idOficina && p.Active)  
                            //.Where(p=> p.UserId == id)
                            .Select(p => new
                            {
                                Id = p.OperationCadastroVeiculosId,
                                Name = p.Marca,
                                Email = p.Modelo + " - " + p.Placa
                            })
                            .ToList()
                            .Where(m => string.IsNullOrEmpty(query) || m.Name.StartsWith(query))
                            .OrderBy(m => m.Name);

                return Json(users, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ApplyUser(Guid ids)
            {
                FormulariodeServicos carrinho = Session["Carrinho"] != null ? (FormulariodeServicos)Session["Carrinho"] : new FormulariodeServicos();

                if (carrinho.LoadForms == null)
                {
                    FormulariodeServicos formulariodeServicos = new FormulariodeServicos();
                    carrinho = formulariodeServicos;
                    carrinho.SystemUser = _context.EfSystemUser.Where(p => p.UserId == ids).FirstOrDefault();
                    carrinho.UserId = ids;
                    Session["Carrinho"] = carrinho;
                }
                else {

                    carrinho.SystemUser = _context.EfSystemUser.Where(p => p.UserId == ids).FirstOrDefault();
                    Session["Carrinho"] = carrinho;
                }
            

                return null;
            }
            public ActionResult ApplyCar(Guid ids)
            {
                FormulariodeServicos carrinho =  new FormulariodeServicos();



                carrinho.OperationCadastroVeiculos = _context.EfOperationCadastroVeiculos.Where(p => p.OperationCadastroVeiculosId == ids).FirstOrDefault();




                carrinho.OperationCadastroOficinaId = OficinaId();
                carrinho.OperationCadastroVeiculosId = ids;
        
          
                //Session["Carrinho"] = carrinho;
                return PartialView(carrinho) ;
            }
        #endregion

        [HttpGet]
        public ActionResult Index(string id = "")
        {
            Guid idOficina = Guid.Empty;
            Guid ids = id == "" ? Guid.Empty : Guid.Parse(id);
            //FormulariodeServicos formulariodeServicos = new FormulariodeServicos();

            FormulariodeServicos carrinho = Session["Carrinho"] != null ? (FormulariodeServicos)Session["Carrinho"] : new FormulariodeServicos();
            FormulariodeServicos formulariodeServicos = new FormulariodeServicos();
            try
            {
                //FormulariodeServicos carrinho = Session["Carrinho"] != null ? (FormulariodeServicos)Session["Carrinho"] : new FormulariodeServicos();

                idOficina = OficinaId();
                //formulariodeServicos.OperationCadastroOficinaId = idOficina;
                //formulariodeServicos.SystemUser = _context.EfSystemUser.Where(p => p.User == User.Identity.Name).FirstOrDefault();

                //carrinho = formulariodeServicos;
                //Session["Carrinho"] = carrinho;



                TbSystemUser systemUserClient = new TbSystemUser();
                TbOperationCadastroVeiculos cadastroVeiculos = new TbOperationCadastroVeiculos();
                TbOperationOrdemDeServico operationOrdemDeServico = new TbOperationOrdemDeServico();
                List<TbHistoryServicoes> HistoryServicoes = new List<TbHistoryServicoes>();

                formulariodeServicos.OperationCadastroVeiculos = _context.EfOperationCadastroVeiculos.Where(p => p.OperationCadastroVeiculosId == ids).FirstOrDefault();


                formulariodeServicos.OperationCadastroOficinaId = idOficina;
                formulariodeServicos.OperationCadastroVeiculos = cadastroVeiculos;

                formulariodeServicos.OperationCadastroVeiculosId = formulariodeServicos.OperationCadastroVeiculos.OperationCadastroVeiculosId;
                formulariodeServicos.SystemUser = _context.EfSystemUser.Where(p => p.User == User.Identity.Name).FirstOrDefault();
                formulariodeServicos.SystemUserClient = systemUserClient;
                formulariodeServicos.OperationOrdemDeServico = operationOrdemDeServico;
                formulariodeServicos.HistoryServicoes = HistoryServicoes;
                carrinho = formulariodeServicos;
                Session["Carrinho"] = carrinho;
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }


            //var users = _context.EfCadastroOficina
            //            .Where(p => p.OperationCadastroOficinaId == idOficina)
            //            .FirstOrDefault().SystemUserCollection
            //            .Select(p => new
            //            {
            //                Id = p.UserId,
            //                Name = p.Name,
            //                Email = p.Email
            //            })
            //            .ToList()
            //            .Where(m => string.IsNullOrEmpty(query) || m.Name.StartsWith(query)
            //            .OrderBy(m => m.Name); ;
            //return Json(students, JsonRequestBehavior.AllowGet);
            return View(formulariodeServicos);
        }

        public ActionResult Index(Guid id)
        {
            Guid idOficina = Guid.Empty;
            try
            {
                idOficina = OficinaId();
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
            if (RedirectToLocalOficina() == false)
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }

            FormulariodeServicos carrinho = Session["Carrinho"] != null ? (FormulariodeServicos)Session["Carrinho"] : new FormulariodeServicos();

            TbSystemUser systemUserClient = new TbSystemUser();
            TbOperationCadastroVeiculos cadastroVeiculos = new TbOperationCadastroVeiculos();
            List<TbHistoryServicoes> HistoryServicoes = new List<TbHistoryServicoes>();
            FormulariodeServicos formulariodeServicos = new FormulariodeServicos
            {
                OperationCadastroVeiculos = _context.EfOperationCadastroVeiculos.Where(p => p.OperationCadastroVeiculosId == id).FirstOrDefault()
            };

            formulariodeServicos.OperationCadastroOficinaId = idOficina;
            formulariodeServicos.OperationCadastroVeiculos = cadastroVeiculos;

            formulariodeServicos.OperationCadastroVeiculosId = formulariodeServicos.OperationCadastroVeiculos.OperationCadastroVeiculosId;
            formulariodeServicos.SystemUser = _context.EfSystemUser.Where(p => p.User == User.Identity.Name).FirstOrDefault();
            formulariodeServicos.SystemUserClient = systemUserClient;
            formulariodeServicos.HistoryServicoes = HistoryServicoes;
            carrinho = formulariodeServicos;
            Session["Carrinho"] = carrinho;
            return View(formulariodeServicos);
        }

        [HttpPost]
        public ActionResult CreateOrder(FormulariodeServicos formulariodeServicos)
        {

            FormulariodeServicos carrinho = Session["Carrinho"] != null ? (FormulariodeServicos)Session["Carrinho"] : new FormulariodeServicos();
            ServicosAddModelView servicosAddView = Session["servicosAddView"] != null ? (ServicosAddModelView)Session["servicosAddView"] : new ServicosAddModelView();

            formulariodeServicos.SystemUser = carrinho.SystemUser;
            formulariodeServicos.SystemUserClient = 
                    FormulariodeServicos.CreateAndValidateUser(formulariodeServicos.SystemUserClient, formulariodeServicos.OperationCadastroOficinaId);

            formulariodeServicos.OperationCadastroVeiculos =
                FormulariodeServicos.CreateAndValidateVeiculo(formulariodeServicos.OperationCadastroVeiculos, 
                                                                    formulariodeServicos.OperationCadastroOficinaId, formulariodeServicos.SystemUserClient);
            List<TbHistoryServicoes> HistoryServicoes = new List<TbHistoryServicoes>();
            Guid id = FormulariodeServicos.CreateOrdemService(formulariodeServicos);

            var msg = new
            {
                error = 1
            };

            carrinho.OperationOrdemDeServico = _context.EfOperationOrdemDeServico.Find(id);


            servicosAddView.OrdemDeServicoId = id;
            Session["servicosAddView"] = servicosAddView;
            Session["Carrinho"] = carrinho;

            return RedirectToAction("Servicos", new { id = id });
        }

        [HttpPost]
        public ActionResult Index()
        {

            FormulariodeServicos carrinho = Session["Carrinho"] != null ? (FormulariodeServicos)Session["Carrinho"] : new FormulariodeServicos();


            FormulariodeServicos.CreateOrdemService(carrinho);

            var msg = new {
                error = 1
            };

            return Json(msg);
        }
        [HttpPost]
        public ActionResult UploadFile()
        {
            try
            {
              
                FormulariodeServicos carrinho = Session["Carrinho"] != null ? 
                    (FormulariodeServicos)Session["Carrinho"] : new FormulariodeServicos();

                HttpPostedFileBase hpf = HttpContext.Request.Files["file"] as HttpPostedFileBase;

                FormServicoesAttachment formServicoesAttachment = new FormServicoesAttachment();
                formServicoesAttachment = FormServicoesAttachment.FormServicoesAttachments(hpf);

                List<FormServicoesAttachment> list = new List<FormServicoesAttachment>();
                if (carrinho.LoadForms != null)
                {
                    list.AddRange(carrinho.LoadForms);
                }
                list.Add(formServicoesAttachment);
                carrinho.LoadForms = list;

                Session["Carrinho"] = carrinho;
                var msg = new { msg = "File Uploaded", filename = hpf.FileName, url = formServicoesAttachment.Base64, id = formServicoesAttachment.Id};
                return Json(msg);
            }
            catch (Exception e)
            {
                //If you want this working with a custom error you need to change in file jquery.uploadfile.js, the name of 
                //variable customErrorKeyStr in line 85, from jquery-upload-file-error to jquery_upload_file_error 
                var msg = new { jquery_upload_file_error = e.Message };
                return Json(msg);
            }
        }
        [HttpPost]
        public ActionResult DeleteFile(Guid id, Guid url)
        {
            try
            {

                FormulariodeServicos carrinho = Session["Carrinho"] != null ?
                    (FormulariodeServicos)Session["Carrinho"] : new FormulariodeServicos();
                

                List<FormServicoesAttachment> list = new List<FormServicoesAttachment>();
                if (carrinho.LoadForms != null)
                {
                    var lists = carrinho.LoadForms.Where(p => p.Id == url).FirstOrDefault();
                    carrinho.LoadForms.Remove(lists);
                   
                }
                
                Session["Carrinho"] = carrinho;
                var msg = new { msg = "Delete File " };
                return Json(msg);
            }
            catch (Exception e)
            {
                //If you want this working with a custom error you need to change in file jquery.uploadfile.js, the name of 
                //variable customErrorKeyStr in line 85, from jquery-upload-file-error to jquery_upload_file_error 
                var msg = new { jquery_upload_file_error = e.Message };
                return Json(msg);
            }
        }
        #endregion

        public ActionResult Servicos(Guid id)
        {
            Guid idOficina = Guid.Empty;
            try
            {
                idOficina = OficinaId();
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
            if (RedirectToLocalOficina() == false)
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
            ServicosAddModelView servicosAddView =
                 Session["servicosModelViews"] != null ?
                 (ServicosAddModelView)Session["servicosModelViews"] : new ServicosAddModelView();
            FormulariodeServicos carrinho = Session["Carrinho"] != null ? 
                (FormulariodeServicos)Session["Carrinho"] : new FormulariodeServicos();


            TbSystemUser systemUserClient = new TbSystemUser();
            TbOperationCadastroVeiculos cadastroVeiculos = new TbOperationCadastroVeiculos();
            TbOperationOrdemDeServico OrdemCompra = new TbOperationOrdemDeServico();
            List<TbHistoryServicoes> OperationServicoes = new List<TbHistoryServicoes>();

            OrdemCompra = _context.EfOperationOrdemDeServico.Find(id);

            FormulariodeServicos formulariodeServicos = new FormulariodeServicos
            {
                OperationCadastroVeiculos = _context.EfOperationCadastroVeiculos
                .Where(p => p.Placa == OrdemCompra.HistoryVeiculos.Placa && p.Marca == OrdemCompra.HistoryVeiculos.Marca).FirstOrDefault()
            };

            systemUserClient = _context.EfSystemUser
                .Where(p => p.UserId == OrdemCompra.HistoryVeiculos.UserId).FirstOrDefault() ;

            formulariodeServicos.SystemUser = _context.EfSystemUser
                .Where(p => p.UserId == OrdemCompra.UserId).FirstOrDefault();

            formulariodeServicos.SystemUserClient = systemUserClient;

            formulariodeServicos.HistoryServicoes = OperationServicoes;

            servicosAddView.OrdemDeServicoId = OrdemCompra.OperationOrdemDeServicoId;
            Session["servicosAddView"] = servicosAddView;

            formulariodeServicos.OperationOrdemDeServico = OrdemCompra;

            return View("Index",formulariodeServicos);
        }

        public ActionResult CreateServico() => View();

        [HttpPost]
        public ActionResult CreateServico(ServicosAddModelView servicosTipesModel)
        {
            Guid idOficina = Guid.Empty;
            try
            {
                idOficina = OficinaId();
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
            ServicosAddModelView servicosAddView =
                Session["servicosAddView"] != null ?
                (ServicosAddModelView)Session["servicosAddView"] : new ServicosAddModelView();

            try
            {

                if (ModelState.IsValid)
                {
                    TbOperationServicoes tbSystemTiposDeServicoes = new TbOperationServicoes();

                  
                    var services = _context.EfOperationOrdemDeServico.Find(servicosAddView.OrdemDeServicoId);

                    tbSystemTiposDeServicoes = _context.EfSystemServicoes.Find(servicosTipesModel.ServicosId);

                    TbHistoryServicoes historyServicoes = new TbHistoryServicoes();
                    historyServicoes.HistoryServicoesId = Guid.NewGuid();
                    historyServicoes.Preco = tbSystemTiposDeServicoes.Preco;
                    historyServicoes.Description = tbSystemTiposDeServicoes.Name;

                    TbHistoryVeiculos tbHistoryVeiculos = new TbHistoryVeiculos();
                    tbHistoryVeiculos = services.HistoryVeiculos;
                    tbHistoryVeiculos.HistoryServicoesCollection.Add(historyServicoes);

                    _context.Entry(tbHistoryVeiculos).State = EntityState.Modified;

                    _context.SaveChanges();
                    //Session["servicos"] = null;
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }

            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Servicos", new { id = servicosAddView.OrdemDeServicoId });
        }

        public ActionResult DeleteServico(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfHistoryServicoes.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost, ActionName("DeleteServico")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Guid idOficina = Guid.Empty;
            try
            {
                idOficina = OficinaId();
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }

            ServicosAddModelView servicosAddView =
                 Session["servicosModelViews"] != null ?
                 (ServicosAddModelView)Session["servicosModelViews"] : new ServicosAddModelView();
            var services = _context.EfOperationOrdemDeServico.Find(servicosAddView.OrdemDeServicoId);
            try
            {

                if (ModelState.IsValid)
                {
                    TbHistoryServicoes historyServicoes = new TbHistoryServicoes();

                    historyServicoes = _context.EfHistoryServicoes.Find(id);
                    TbHistoryVeiculos tbHistoryVeiculos = new TbHistoryVeiculos();

                    tbHistoryVeiculos = services.HistoryVeiculos;
                    tbHistoryVeiculos.HistoryServicoesCollection.Remove(historyServicoes);

                    _context.Entry(tbHistoryVeiculos).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }

            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Servicos", new { id = servicosAddView.OrdemDeServicoId });
        }



        public ActionResult SearchServico(string query = "")
        {
            Guid idOficina = Guid.Empty;
            try
            {
                idOficina = OficinaId();
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
            if (query == "")
            {
                var users = _context.EfSystemServicoes.Where(p => p.Active)

                           .Select(p => new
                           {
                               Id = p.OperationServicoesId,
                               Name = p.Name,
                               Email = ""
                           })
                           .ToList()
                          
                           .OrderBy(m => m.Name);
                return Json(users, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var users = _context.EfSystemServicoes.Where(p => p.Active)

                            .Select(p => new
                            {
                                Id = p.OperationServicoesId,
                                Name = p.Name,
                                Email = ""
                            })
                            .ToList()
                            .Where(m => string.IsNullOrEmpty(query) || m.Name.StartsWith(query))
                            .OrderBy(m => m.Name);
                return Json(users, JsonRequestBehavior.AllowGet);
            }
        
        }


        public ActionResult FinalizarPedido(Guid id)
        {
            var ordem = _context.EfOperationOrdemDeServico.Where(p => p.OperationOrdemDeServicoId == id).FirstOrDefault();

            ordem.Active = false;


            _context.Entry(ordem).State = EntityState.Modified;

            _context.SaveChanges();
            ShowSuccessMessage("Ordem Concluida");
            var a = SendEmail.SendEmailsConfirmEmail(ordem.HistoryVeiculos.SystemClient.Email, "Ordem nº " + ordem.OrdemDeServico  + "</br> Finalizada ", "");
            return RedirectToAction("Index", "OrdemDeServicoes"); ;
        }

        #region Cliente_Manutenção
        public ActionResult CreateCliente()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCliente(TbSystemUser tbSystemUser)
        {
            Guid idOficina = Guid.Empty;
            try
            {
                idOficina = OficinaId();
            }
            catch
            {
                ShowDangerMessage(SystemConstants.OficinaError);
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
            try
            {

                if (ModelState.IsValid)
                {
                    TbSystemUser tbSystemUsers = _context.EfSystemUser.Where(p => p.Email == tbSystemUser.Email).FirstOrDefault();
                    if (tbSystemUsers == null)
                    {
                        tbSystemUser.UserId = Guid.NewGuid();
                        tbSystemUser.Senha = Guid.NewGuid().ToString();
                        tbSystemUser.LastAccess = DateTime.Now;
                        tbSystemUser.OficinaCollection.Add(_context.EfCadastroOficina.Where(p => p.OperationCadastroOficinaId == idOficina).FirstOrDefault());
                        _context.EfSystemUser.Add(tbSystemUser);
                        _context.SaveChanges();
                    }
                    else
                    {
                        tbSystemUsers.OficinaCollection.Add(_context.EfCadastroOficina.Where(p => p.OperationCadastroOficinaId == idOficina).FirstOrDefault());
                        _context.EfSystemUser.Add(tbSystemUsers);
                        _context.Entry(tbSystemUser).State = EntityState.Modified;
                        _context.SaveChanges();

                    }
                    ShowSuccessMessage("Cliente Criado");
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult DeleteCliente(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfSystemUser.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost, ActionName("DeleteCliente")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedCliente(Guid id)
        {
            try
            {
                var tbOperationOficina = _context.EfSystemUser.Find(id);
                tbOperationOficina.Active = false;
                _context.EfSystemUser.Add(tbOperationOficina);
                _context.Entry(tbOperationOficina).State = EntityState.Modified;
                _context.SaveChanges();
                ShowSuccessMessage("Cliente Deletado");
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        #endregion

        #region Veiculo_Manutenção

        public ActionResult CreateVeiculo()
        {
            Guid id = OficinaId();
            TbOperationCadastroVeiculos tbOperationCadastro = new TbOperationCadastroVeiculos();
            tbOperationCadastro.OperationCadastroOficinaId = id;
            return View(tbOperationCadastro);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateVeiculo(TbOperationCadastroVeiculos tbOperationCadastroVeiculos)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbOperationCadastroVeiculos.OperationCadastroVeiculosId = Guid.NewGuid();
                    tbOperationCadastroVeiculos.OperationCadastroOficinaId = OficinaId();
                    tbOperationCadastroVeiculos.Created = DateTime.Now;
                    tbOperationCadastroVeiculos.Modified = DateTime.Now;

                    tbOperationCadastroVeiculos.UserId = _context.EfSystemUser.Where(p => p.UserId == tbOperationCadastroVeiculos.UserId).FirstOrDefault().UserId;
                    _context.EfOperationCadastroVeiculos.Add(tbOperationCadastroVeiculos);
                    _context.SaveChanges();
                    ShowSuccessMessage("Veiculo Criado");
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }


        public ActionResult DeleteVeiculo(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfOperationCadastroVeiculos.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }

            return View(tbCompanies);
        }

        [HttpPost, ActionName("DeleteVeiculo")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedVeiculo(Guid id)
        {
            try
            {
                var tbOperationCadastroVeiculos = _context.EfOperationCadastroVeiculos.Find(id);
                tbOperationCadastroVeiculos.Active = false;
                _context.EfOperationCadastroVeiculos.Add(tbOperationCadastroVeiculos);
                _context.Entry(tbOperationCadastroVeiculos).State = EntityState.Modified;
                _context.SaveChanges();
                ShowSuccessMessage("Veiculo Deletado");
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        #endregion
    }
}
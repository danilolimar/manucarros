﻿
//using BusinessLayer.Commom;
//using BusinessLayer.Security;
//using DataLayer.DbSystem.Context;
//using DataLayer.DbSystem.Models;
//using System;
//using System.Data.Entity;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;
//using System.Web.Security;

//namespace Pegasus.Areas.Support.Controllers
//{
//    [SystemAuthorize(Roles = "SUPPORT")]
//    public class CompaniesController : BaseController
//    {
//        DbSystemContext _context = new DbSystemContext();

//        public CompaniesController()
//        {

//        }
//        public ActionResult Index()
//        {
//            return View(_context.EfSystemCompanies.ToList());
//        }

//        public ActionResult Details(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var tbCompanies = _context.EfSystemCompanies.Find(id);
//            if (tbCompanies == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tbCompanies);
//        }

//        public ActionResult Create()
//        {
//            return View();
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "CompanyId,Code,Name,Active")] TbSystemCompanies tbCompanies)
//        {
//            try
//            {
//                if (ModelState.IsValid)
//                {
//                    tbCompanies.CompanyId = Guid.NewGuid();
//                    _context.EfSystemCompanies.Add(tbCompanies);
//                    _context.SaveChanges();
//                }
//                else
//                {
//                    ShowDangerMessage(ModelState);
//                }
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex);
//            }
//            return RedirectToAction("Index");
//        }

//        public ActionResult Edit(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var tbCompanies = _context.EfSystemCompanies.Find(id);
//            if (tbCompanies == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tbCompanies);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "CompanyId,Code,Name,Active")] TbSystemCompanies tbCompanies)
//        {
//            try
//            {
//                if (ModelState.IsValid)
//                {
//                    _context.Entry(tbCompanies).State = EntityState.Modified;
//                    _context.SaveChanges();
//                }
//                else
//                {
//                    ShowDangerMessage(ModelState);
//                }
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex.InnerException.Message);
//            }
//            return RedirectToAction("Index");
//        }

//        public ActionResult Delete(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var tbCompanies = _context.EfSystemCompanies.Find(id);
//            if (tbCompanies == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tbCompanies);
//        }

//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(Guid id)
//        {
//            try
//            {
//                var tbCompanies = _context.EfSystemCompanies.Find(id);
//                tbCompanies.Active = true;
//                _context.EfSystemCompanies.Remove(tbCompanies);
//                _context.SaveChanges();
//            }
//            catch (Exception ex)
//            {
//                ShowDangerMessage(ex.InnerException.Message);
//            }
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                _context.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}
﻿using BusinessLayer.Commom;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Net;
using System.Web.Mvc;

namespace Pegasus.Areas.Support.Controllers
{
    [SystemAuthorize(Roles = "SUPORTE")]
    public class PermissionsController : BaseController
    {
        DbSystemContext _context = new DbSystemContext();

        public PermissionsController()
        {
            _context = new DbSystemContext();
        }

        public ActionResult Index()
        {
            return View(_context.EfSystemPermissions);
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbPermissions = _context.EfSystemPermissions.Find(id);
            if (tbPermissions == null)
            {
                return HttpNotFound();
            }
            return View(tbPermissions);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PermissionId,Name,Description,Active")] TbSystemPermissions tbPermissions)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbPermissions.PermissionId = Guid.NewGuid();
                    tbPermissions.Name = tbPermissions.Name.TrimToUpper();
                    tbPermissions.Description = tbPermissions.Description.TrimToUpper();
                    _context.EfSystemPermissions.Add(tbPermissions);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbPermissions = _context.EfSystemPermissions.Find(id);
            if (tbPermissions == null)
            {
                return HttpNotFound();
            }
            return View(tbPermissions);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PermissionId,Name,Description,Active")] TbSystemPermissions tbPermissions)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.EfSystemPermissions.Add(tbPermissions);
                    _context.Entry(tbPermissions).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbPermissions = _context.EfSystemPermissions.Find(id);
            if (tbPermissions == null)
            {
                return HttpNotFound();
            }
            return View(tbPermissions);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                var tbPermissions = _context.EfSystemPermissions.Find(id);
                tbPermissions.Active = false;
                _context.EfSystemPermissions.Add(tbPermissions);
                _context.Entry(tbPermissions).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
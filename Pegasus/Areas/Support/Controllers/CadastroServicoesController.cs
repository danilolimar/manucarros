﻿
using BusinessLayer.Commom;
using BusinessLayer.Security;
using DataLayer.DbSystem.Context;
using DataLayer.DbSystem.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;

namespace Pegasus.Areas.Support.Controllers
{
    [SystemAuthorize(Roles = "SUPORTE")]
    public class CadastroServicoesController : BaseController
    {
        DbSystemContext _context = new DbSystemContext();

        public CadastroServicoesController()
        {

        }
        public ActionResult Index()
        {
            //Guid idOficina = Guid.Empty;
            //try
            //{
            //    idOficina = OficinaId();
            //}
            //catch
            //{
            //    ShowDangerMessage(SystemConstants.OficinaError);
            //    return RedirectToAction("Index", "Home", new { Area = "" });
            //}

            //if (RedirectToLocalOficina() == false)
            //{
            //    ShowDangerMessage(SystemConstants.OficinaError);
            //    return RedirectToAction("Index", "Home", new { Area = "" });
            //}

            return View(_context.EfSystemServicoes.ToList());
        }


        public ActionResult Create()
        {
            ViewBag.id = OficinaId();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TbOperationServicoes SystemTiposDeServicoes)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SystemTiposDeServicoes.OperationServicoesId = Guid.NewGuid();
                    SystemTiposDeServicoes.Modified = DateTime.Now;
                    SystemTiposDeServicoes.Created = DateTime.Now;
                    //SystemTiposDeServicoes.OperationCadastroOficinaId = OficinaId();
                    SystemTiposDeServicoes.UserId = _context.EfSystemUser.Where(P => P.User == User.Identity.Name).FirstOrDefault().UserId;
                    _context.EfSystemServicoes.Add(SystemTiposDeServicoes);
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfSystemServicoes.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TbOperationServicoes SystemTiposDeServicoes)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SystemTiposDeServicoes.Modified = DateTime.Now;
                    _context.Entry(SystemTiposDeServicoes).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {
                    ShowDangerMessage(ModelState);
                }
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbCompanies = _context.EfSystemServicoes.Find(id);
            if (tbCompanies == null)
            {
                return HttpNotFound();
            }
            return View(tbCompanies);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                var tbOperationOficina = _context.EfSystemServicoes.Find(id);
                tbOperationOficina.Active = false;
                _context.EfSystemServicoes.Add(tbOperationOficina);
                _context.Entry(tbOperationOficina).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ShowDangerMessage(ex.InnerException.Message);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}